import org.apache.solr.SolrTestCaseJ4;
import org.apache.solr.request.SolrQueryRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public abstract class AbstractAqlParserTest extends SolrTestCaseJ4 {
  protected static final String[] defaultParams = new String[]{
      "df", "content_txt",
      "indent", "true",
      "sort", "score desc, id asc",
      "q.op", "AND",
      "debugQuery", "true",
      "fl", "*,score",
      "hl", "true",
      "hl.method", "unified",
      "rows", "100"
  };
  
  protected abstract String getDefType();

  // Asserts that the count and ids match the query response.
  protected void assertResults(String q, int resultCount, String... ids) {
    assertResults(defaultParams, q, getDefType(), resultCount, false, ids);
  }
  protected void assertResults(String[] params, String q, int resultCount, String... ids) {
    assertResults(params, q, getDefType(), resultCount, false, ids);
  }
  protected void assertResults(String q, int resultCount, boolean sortedIds, String... ids) {
    assertResults(defaultParams, q, getDefType(), resultCount, sortedIds, ids);
  }
  protected void assertResults(String[] params, String q, int resultCount, boolean sortedIds, String... ids) {
    assertResults(params, q, getDefType(), resultCount, sortedIds, ids);
  }
  protected void assertResults(String q, String defType, int resultCount, String... ids) {
    assertResults(defaultParams, q, defType, resultCount, false, ids);
  }
  protected void assertResults(String[] params, String q, String defType, int resultCount, boolean sortedIds, String... ids) {
    SolrQueryRequest req = req(params, "q", q, "defType", defType);
    List<String> tests = new ArrayList<>();
    tests.add(String.format("/response/result[count(doc) = %d]", resultCount));
    if (ids.length > 0) {
      if (sortedIds) {
        AtomicInteger i = new AtomicInteger();
        tests.addAll(Arrays.stream(ids).map(id -> String.format("/response/result/doc[%d]/str[@name='id'][text()='%s']", i.incrementAndGet(), id)).collect(Collectors.toList()));
      } else {
        tests.addAll(Arrays.stream(ids).map(id -> String.format("/response/result/doc/str[@name='id'][text()='%s']", id)).collect(Collectors.toList()));
      }
    }
    assertQ(req, tests.toArray(new String[0]));
  }

}
