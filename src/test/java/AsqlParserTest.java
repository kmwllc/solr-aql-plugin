import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AsqlParserTest extends AbstractAqlParserTest {
  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-M-d'T'HH:mm:ss.SSSX");
  static {
    DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
  }

  @BeforeClass
  public static void beforeTests() throws Exception {
    File confDir = new File("src/test/resources/configsets/aql_configs/conf");
    File coreDir = confDir.getParentFile();
    File homeDir = coreDir.getParentFile();
    System.setProperty("solr.directoryFactory", "solr.NRTCachingDirectoryFactory");
    initCore("solrconfig.xml", "managed-schema", homeDir.getPath(), coreDir.getName());
  }

  @Before
  public void clearAllDocs() {
    clearIndex();
  }

  protected String getDefType() {
    return "asql";
  }
  
  @Test
  public void testBasic() {
    assertU(adoc("id", "1", "content_txt", "1 - aaa of bbb ccc 12345"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - foo bar aaafoo"));
    assertU(commit());

    assertResults("content_txt:12345", 1, "1");
    assertResults("content_txt:12345", "edismax", 1, "1");

    assertResults("aaa", 3, "1", "5", "6");
    assertResults("aaa", "edismax", 3, "1", "5", "6");

    assertResults("aa*", 4, "1", "5", "6", "7");
    assertResults("aa*", "edismax", 4, "1", "5", "6", "7");

    assertResults("aaa bbb", 2, "1", "5");
    assertResults("aaa bbb", "edismax", 2, "1", "5");

    assertResults("aaa AND bbb", 2, "1", "5");
    assertResults("aaa AND bbb", "edismax", 2, "1", "5");

    assertResults("aaa OR bbb", 4, "1", "2", "5", "6");
    assertResults("aaa OR bbb", "edismax", 4, "1", "2", "5", "6");

    assertResults("-content_txt:aaa", 4, "2", "3", "4", "7");
    assertResults("NOT aaa", 4, "2", "3", "4", "7");
    assertResults("NOT aaa", "edismax", 4, "2", "3", "4", "7");
    assertResults("-aaa", 4, "2", "3", "4", "7");
    assertResults("-aaa", "edismax", 4, "2", "3", "4", "7");

    assertResults("NOT (aaa OR bbb)", 3, "3", "4", "7");
    assertResults("NOT (aaa OR bbb)", "edismax", 3, "3", "4", "7");

    assertResults("NOT aaa NOT bbb", 3, "3", "4", "7");
    assertResults("NOT aaa NOT bbb", "edismax", 3, "3", "4", "7");
    assertResults("NOT aaa AND NOT bbb", 3, "3", "4", "7");
    assertResults("NOT aaa AND NOT bbb", "edismax", 3, "3", "4", "7");
    assertResults("-aaa -bbb", 3, "3", "4", "7");
    assertResults("-aaa -bbb", "edismax", 3, "3", "4", "7");
    assertResults("-aaa AND -bbb", 3, "3", "4", "7");
    assertResults("-aaa AND -bbb", "edismax", 3, "3", "4", "7");

    assertResults("NOT aaa NOT bbb caa", 1, "3");
    assertResults("NOT aaa NOT bbb caa", "edismax", 1, "3");


    assertResults("\"caa cbb\"", 2, "3", "5");
    assertResults("-\"caa cbb\"", 5, "1", "2", "4", "6", "7");
    assertResults("-content_txt:\"caa cbb\"", 5, "1", "2", "4", "6", "7");

    // edismax interprets this as a negative, we inherit the behavior.
    assertResults("-content_txt:-aaa", 4, "2", "3", "4", "7");
    assertResults("-content_txt:-aaa", "edismax",4, "2", "3", "4", "7");

    // inconsistently, edismax interprets this as a positive when it's a phrase, we inherit the behavior.
    assertResults("-content_txt:-\"caa cbb\"", 2, "3", "5");
    assertResults("-content_txt:-\"caa cbb\"", "edismax", 2, "3", "5");
  }
  
  @Test
  public void testQueryWithSpecialChars() {
    assertU(adoc("id", "1", "content_txt", "1 - aaa of bbb ccc 12345"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - foo, bar aaafoo"));
    assertU(commit());

    assertResults("foo, bar", 3, "3", "6", "7");
    assertResults("foo| bar", 3, "3", "6", "7");
    assertResults("foo& bar", 3, "3", "6", "7");
    assertResults("foo! bar", 3, "3", "6", "7");
  }
  
  @Test
  public void testSynonymExpansion() {
    assertU(adoc("id", "1", "content_txt", "1 - aaabar baraaa of bbb ccc"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - aaafoo fooaaa caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - aaabar fooaaa of bbb ccc"));
    assertU(adoc("id", "8", "content_txt", "8 - home work done"));
    assertU(adoc("id", "9", "content_txt", "9 - homework done"));
    assertU(adoc("id", "10", "content_txt", "10 - home is where the work is done"));
    assertU(commit());

    // directional synonym aaafoo=>aaabar (should not return id:3)
    assertResults("aAaFoo OR bbb", 4, "1", "2", "5", "7");

    // expansion synonym fooaaa,baraaa,bazaaa
    assertResults("fOoAaa OR bbb", 5, "1", "2", "3", "5", "7");
    assertResults("\"aaabar baraaa\"", 2, "1", "7");
    assertResults("\"aaabar baraaa\"", "edismax", 2, "1", "7");

    // test multi-word
    assertResults("\"homework\"", 2, "8", "9");
    assertResults("\"homework\"", "edismax", 2, "8", "9");
    assertResults("\"home work\"", 2, "8", "9");
    assertResults("\"home work\"", "edismax", 2, "8", "9");
    assertResults("\"home work done\"", 2, "8", "9");
    assertResults("\"home work done\"", "edismax", 2, "8", "9");
    
    // multi-word synonyms are not expanded in asql, so 9 is not found.
    //assertResults("home work", 3, "8", "9", "10");
    assertResults("home work", "edismax", 3, "8", "9", "10");
    //assertResults("home work done", 3, "8", "9", "10");
    assertResults("home work done", "edismax", 3, "8", "9", "10");

    // edismax & lucene both return doc 10 here and probably shouldn't. 
    // The query becomes an And on the individual words of the multi-word synonym instead of a phrase: 
    // +((+content_txt:home +content_txt:work) content_txt:homework)
    //assertResults("homework", "edismax", 2, "8", "9");
    //assertResults("homework", "lucene", 2, "8", "9");
    //assertResults("homework", 2, "8", "9");

    // do not expand the synonym here.
    assertResults("home AND work AND done", 2, "8", "10");
    assertResults("home AND work AND done", "lucene", 2, "8", "10");
    assertResults("home AND work AND done", "edismax", 2, "8", "10");
  }
  
  @Test
  public void testRange() {
    assertU(adoc("id", "1", "content_txt", "1 - aaa of bbb ccc 12345"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - foo bar aaafoo"));
    assertU(commit());

    assertResults("content_txt:>foo", 2, "1", "3");
    assertResults("content_txt:{foo TO *]", "edismax", 2, "1", "3");
    
    assertResults("-content_txt:>foo", 5, "2", "4", "5", "6", "7");

    assertResults("content_txt:>=foo", 4, "1", "3", "6", "7");
    assertResults("content_txt:[foo TO *]", "edismax", 4, "1", "3", "6", "7");
  }

  @Test
  public void testNumericRange() {
    assertU(adoc("id", "1", "content_txt", "1 - aaa of bbb ccc 12345", "content_d", "345"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - foo bar aaafoo"));
    assertU(adoc("id", "8", "content_d", "8"));
    assertU(adoc("id", "9", "content_d", "999"));
    assertU(adoc("id", "10", "content_d", "10.0001"));
    assertU(adoc("id", "11", "content_d", "10.99999999999999"));
    assertU(adoc("id", "12", "content_d", "1200000000"));
    assertU(adoc("id", "13", "content_d", "-13"));
    assertU(adoc("id", "14", "content_d", "-1400"));
    assertU(commit());

    assertResults("content_d:>11", 3, "1", "9", "12");
    assertResults("content_d:{11 TO *]", "edismax", 3, "1", "9", "12");
    
    assertResults("-content_d:>11", 11, "2", "3", "4", "5", "6", "7", "8", "10", "11", "13", "14");

    assertResults("content_d:>-13", 6, "1", "8", "9", "10", "11", "12");
    assertResults("content_d:{-13 TO *]", "edismax", 6, "1", "8", "9", "10", "11", "12");

    assertResults("content_d:>=-13", 7, "1", "8", "9", "10", "11", "12", "13");
    assertResults("content_d:[-13 TO *]", "edismax", 7, "1", "8", "9", "10", "11", "12", "13");

    assertResults("content_d:<20.1", 5, "8", "10", "11", "13", "14");
    assertResults("content_d:[* TO 20.1}", "edismax", 5, "8", "10", "11", "13", "14");

    assertResults("content_d:<=8", 3, "8", "13", "14");
    assertResults("content_d:[* TO 8]", "edismax", 3, "8", "13", "14");

    assertResults("content_d:>=100 content_txt:aaa", 1, "1");
    assertResults("content_d:[100 TO *] content_txt:aaa", "edismax", 1, "1");

    assertResults("content_d:>=100 aaa", 1, "1");
    assertResults("content_d:[100 TO *] aaa", "edismax", 1, "1");

    assertResults("content_d:>=100 (aaa OR bbb)", 1, "1");
    assertResults("content_d:[100 TO *] (aaa OR bbb)", "edismax", 1, "1");

    assertResults("(content_d:>=100 aaa) OR \"bbb bcc\"", 2, "1", "2");
    assertResults("(content_d:[100 TO *] aaa) OR \"bbb bcc\"", "edismax", 2, "1", "2");
  }

  @Test
  public void testDateRange() {
    assertU(adoc("id", "1",   "created_dt", "2021-11-02T10:35:00.000Z", "content_txt", "1 - aaa of bbb ccc 12345"));
    assertU(adoc("id", "2",   "created_dt", "2021-11-02T10:45:00.000Z", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3",   "created_dt", "2021-11-02T10:45:23.456Z", "content_txt", "3 - baa bbb bcc"));
    assertU(adoc("id", "4",   "created_dt", "2021-11-02T10:55:00.000Z", "content_txt", "4 - caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "5",   "created_dt", "2021-11-02T11:00:00.000Z", "content_txt", "5 - daa dbb dcc"));
    assertU(adoc("id", "6",   "created_dt", "2021-11-01T10:35:00.000Z", "content_txt", "6 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "7",   "created_dt", "2021-10-02T10:35:00.000Z", "content_txt", "7 - foo bar aaa"));
    assertU(adoc("id", "8",   "created_dt", "2020-11-02T10:35:00.000Z", "content_txt", "8 - foo bar aaafoo"));
    assertU(adoc("id", "9",   "created_dt", "2022-11-02T10:35:00.000Z", "content_txt", "8 - foo bar aaafoo"));
    assertU(commit());

    assertResults("created_dt:\"2021-11-02T10:45:23.456Z\"", 1, "3");
    assertResults("created_dt:\"2021-11-02T10:45:23.456\"", 1, "3");
    assertResults("created_dt:\"2021-11-02T10:45:23\"", 1, "3");
    assertResults("created_dt:\"2021-11-02T10:45\"", 2, "2", "3");
    assertResults("created_dt:2021-11-02T10", 4, "1", "2", "3", "4");
    assertResults("created_dt:\"2021-11-02T10\"", 4, "1", "2", "3", "4");
    assertResults("created_dt:2021-11-02", 5, "1", "2", "3", "4", "5");
    assertResults("created_dt:\"2021-11-02\"", 5, "1", "2", "3", "4", "5");

    assertResults("created_dt:>2021-11-02", 1, "9");
    assertResults("created_dt:<2021-11-02", 3, "6", "7", "8");
    assertResults("created_dt:<2021-11-02T11", 7, "1", "2", "3", "4", "6", "7", "8");
    assertResults("created_dt:>=2021-11-02", 6, "1", "2", "3", "4", "5", "9");
    assertResults("created_dt:[2021-11-02T00:00:00.000Z TO *}", "edismax", 6, "1", "2", "3", "4", "5", "9");

    assertResults("created_dt:>=2021-11", 7, "1", "2", "3", "4", "5", "6", "9");
    assertResults("created_dt:>=2021/11", 7, "1", "2", "3", "4", "5", "6", "9");
    
    assertResults("created_dt:>2021-11", 1, "9");
    assertResults("created_dt:>2021/11", 1, "9");
  }

  @Test
  public void testDateMath() {
    Calendar cal = Calendar.getInstance();
    assertU(adoc("id", "1", "content_txt", "1 - aaa of bbb ccc 12345", "created_dt", DATE_FORMAT.format(cal.getTime())));
    
    cal.setTime(new Date());
    cal.add(Calendar.HOUR, -24);
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc", "created_dt", DATE_FORMAT.format(cal.getTime())));
    
    cal.setTime(new Date());
    cal.add(Calendar.HOUR, -24*3);
    assertU(adoc("id", "3", "content_txt", "3 - caa cbb ccc foo bar baz fooo barr bazz", "created_dt", DATE_FORMAT.format(cal.getTime())));

    cal.setTime(new Date());
    cal.add(Calendar.HOUR, -24*10);
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb dcc", "created_dt", DATE_FORMAT.format(cal.getTime())));

    cal.setTime(new Date());
    cal.add(Calendar.HOUR, -24*65);
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc", "created_dt", DATE_FORMAT.format(cal.getTime())));

    cal.setTime(new Date());
    cal.add(Calendar.MONTH, 13);
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa", "created_dt", DATE_FORMAT.format(cal.getTime())));

    cal.setTime(new Date());
    cal.add(Calendar.MONTH, -25);
    assertU(adoc("id", "7", "content_txt", "7 - foo bar aaafoo", "created_dt", DATE_FORMAT.format(cal.getTime())));

    assertU(commit());

    assertResults("created_dt:LAST1DAYS", 2, "1", "2");
    assertResults("created_dt:[NOW/DAY-1DAY TO NOW]", "edismax", 2, "1", "2");

    assertResults("created_dt:LAST7DAYS", 3, "1", "2", "3");
    assertResults("created_dt:[NOW/DAY-7DAYS TO NOW]", "edismax", 3, "1", "2", "3");

    assertResults("created_dt:LASTMONTH", 4, "1", "2", "3", "4");
    assertResults("created_dt:[NOW/DAY-1MONTH TO NOW]", "edismax", 4, "1", "2", "3", "4");
    
    assertResults("created_dt:LAST4MONTHS", 5, "1", "2", "3", "4", "5");
    assertResults("created_dt:[NOW/DAY-4MONTHS TO NOW]", "edismax", 5, "1", "2", "3", "4", "5");
  }

  @Test
  public void testPhraseGap() throws Exception {
    assertU(adoc("id", "1", "content_txt", "1 - drive the car home"));
    assertU(adoc("id", "2", "content_txt", "2 - drive any car home"));
    assertU(adoc("id", "3", "content_txt", "1 - drive the the car home"));
    assertU(adoc("id", "4", "content_txt", "2 - drive the any car home"));
    assertU(adoc("id", "5", "content_txt", "1 - drive the the the car home"));
    assertU(adoc("id", "6", "content_txt", "2 - drive the any any car home"));
    assertU(commit());

    // matches both documents because the stopword is replaced with a gap
    assertResults("\"drive the car\"", 2, "1", "2");
    assertResults("\"drive the the car\"", 2, "3", "4");
    assertResults("\"drive the the the car\"", 2, "5", "6");
    
    assertResults("-\"drive the car\"", 4, "3", "4", "5", "6");

    // test edismax does the same
    assertResults("\"drive the car\"", "edismax", 2, "1", "2");
    assertResults("\"drive the the car\"", "edismax", 2, "3", "4");
    assertResults("\"drive the the the car\"", "edismax", 2, "5", "6");
  }

  @Test
  public void testStopWords() throws Exception {
    assertU(adoc("id", "1", "content_txt", "1 - aaabar baraaa of bbb ccc"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - aaafoo fooaaa caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb the dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(commit());

    assertResults("the", 0);
    assertResults("-the bbb", 3, "1", "2", "5");
    assertResults("\"the\" bbb ", 3, "1", "2", "5");
    assertResults("the OR bbb", 3, "1", "2", "5");
  }

  @Test
  public void testWildcards() throws Exception {
    assertU(adoc("id", "1", "content_txt", "1 - aaabar baraaa of bbb ccc"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - aaafoo fooaaa caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb the dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - foo bar baz"));
    assertU(adoc("id", "8", "content_txt", "8 - aaabar bara* of bbb ccc"));
    assertU(commit());

    assertResults("*", 8, "1", "2", "3", "4", "5", "6", "7", "8");

    assertResults("aaa*", 5, "1", "3", "5", "6", "8");

    // synonyms are not expanded on wildcards (otherwise, document 1 would be returned).
    assertResults("fooaa*", 1, "3");
    assertResults("fooaa*", "lucene", 1, "3");
    assertResults("fooaa*", "edismax", 1, "3");
    
    assertResults("-fooaa*", 7, "1", "2", "4", "5", "6", "7");
    assertResults("-fooaa*", "lucene", 7, "1", "2", "4", "5", "6", "7");
    assertResults("-fooaa*", "edismax", 7, "1", "2", "4", "5", "6", "7");

    assertResults("?aa",  5, "2", "3", "4", "5", "6");
    assertResults("?aa", "lucene", 5, "2", "3", "4", "5", "6");

    assertResults("?aa*",  7, "1", "2", "3", "4", "5", "6", "8");
    assertResults("?aa*", "lucene", 7, "1", "2", "3", "4", "5", "6", "8");

    assertResults("*aa foo",  2, "3", "6");
    assertResults("*aa AND foo",  "lucene", 2, "3", "6");

    assertResults("aaAbar baRa*", 2, "1", "8");
    assertResults("aaabar bara*", "edismax", 2, "1", "8");
    assertResults("aaabar bara*", "lucene", 2, "1", "8");

    assertResults("baRaa*", "edismax", 1, "1");
    assertResults("baRaa*", "aql", 1, "1");
  }

  @Test
  public void testWildcardPhrased() throws Exception {
    assertU(adoc("id", "1", "content_txt", "1 - aaabar baraaa of bbb ccc"));
    assertU(adoc("id", "1a", "content_txt", "1a - aaabar of bbb baraaa ccc"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - aaafoo fooaaa caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb the dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - foo bar baz"));
    assertU(adoc("id", "8", "content_txt", "8 - aaabar bara* of bbb ccc"));
    assertU(adoc("id", "9", "content_txt", "9 - aaa?ar bara* of bbb ccc"));
    assertU(adoc("id", "9a", "content_txt", "9a - aaa ar bara* of bbb ccc"));
    assertU(adoc("id", "9b", "content_txt", "9b - aaa?ar bara of bbb ccc"));
    assertU(adoc("id", "9c", "content_txt", "9c - aaa arasdf bara of bbb ccc"));
    assertU(adoc("id", "10", "content_txt", "10 - aaa?ar bara\\* of bbb ccc"));
    assertU(adoc("id", "11", "content_txt", "11 - foo home work baraaa of bbb ccc"));
    assertU(adoc("id", "11a", "content_txt", "11a - foo homework baraaa of bbb ccc"));
    assertU(adoc("id", "12", "content_txt", "12 - aaabar bara\\* of bbb ccc"));
    assertU(adoc("id", "13", "content_txt", "13 - fooaaa bar baz"));
    assertU(adoc("id", "14", "content_txt", "14 - fooaaazbar baz"));
    assertU(adoc("id", "15", "content_txt", "15 - baraaa bar baz"));
    assertU(adoc("id", "16", "content_txt", "16 - baraaazbar baz"));
    assertU(adoc("id", "17", "content_txt_en_split", "17 - University Arizona Central Receiving"));
    assertU(adoc("id", "18", "content_txt_en_split", "18 - Univ Arizona Lunar & Planetary Lab"));
    assertU(adoc("id", "19", "content_txt_en_split", "19 - Univ Of Arizona"));
    assertU(adoc("id", "20", "content_txt_en_split", "20 - University Of Arizona"));
    assertU(commit());

    assertResults("content_txt_en_split:\"Univ* Of Arizona\"", 2, "19", "20");
    
    assertResults("\"aaabar bara*\"", 3, "1", "8", "12");
    assertResults("\"aaabar bara*\"", "edismax", 2, "8", "12");
    assertResults("\"aaabar bara*\"", "lucene", 2, "8", "12");
    
    assertResults("-\"aaabar bara*\"", 22, "1a", "2", "3", "4", "5", "6", "7", "9", "9a", "9b", "9c", "10", "11", "11a", "13", "14", "15", "16", "17", "18", "19", "20");
    assertResults("-content_txt:\"aaabar bara*\"", 22, "1a", "2", "3", "4", "5", "6", "7", "9", "9a", "9b", "9c", "10", "11", "11a", "13", "14", "15", "16", "17", "18", "19", "20");
    assertResults("-content_txt:-\"aaabar bara*\"", 3, "1", "8", "12"); // reproduce the inconsistency documented in testBasic().
    
    assertResults("aaabar -content_txt:\"bbb cc*\"", 1, "1a");

    // wildcards in both cases
    assertResults("aaa?ar", 4, "1", "1a", "8", "12");
    assertResults("aaa?ar", "edismax", 4, "1", "1a", "8", "12");

    // only wildcard in AQL
    assertResults("\"aaa?ar\"", 4, "1", "1a", "8", "12");
    assertResults("\"aaa?ar\"", "edismax", 4, "9", "9a", "9b", "10");

    // not a wildcard - ? is escaped
    assertResults("\"aaa\\?ar\"", 4, "9", "9a", "9b", "10");
    assertResults("\"aaa\\?\"", 7, "5", "6", "9", "9a", "9b", "9c", "10");

    // fooaaa synonym is not expanded as it's part of the wildcard term
    assertResults("fOoAaa?bar", 1, "14");
    assertResults("\"fOoAaa?bar\"", 1, "14");

    // homework synonym is expanded
    assertResults("\"foo hOmeWork bar*\"", 2, "11", "11a");
    assertResults("\"hOmeWork b*\"", 2, "11", "11a");

    // only single-word synonyms are expanded
    assertResults("\"home work bara*\"", 1, "11");
    assertResults("\"homework bara*\"", 2, "11", "11a");

    assertResults("\"aAa a*\"", 5, "9", "9a", "9b", "9c", "10");
    assertResults("\"aaa\\? a*\"", 5, "9", "9a", "9b", "9c", "10");
    assertResults("aaa\\?a*", "edismax", 0);
    assertResults("\"aaa\\?a*\"", 0);
    assertResults("\"aaa\\* a*\"", 5, "9", "9a", "9b", "9c", "10");

    // edismax returns 1a because it's an AND not a phrase
    assertResults("\"aaa?ar bara*\"", 3, "1", "8", "12");
    assertResults("aaa?ar bara*", "edismax", 4, "1", "1a", "8", "12");
  }
  
  @Test
  public void testWildcardPhrasedStemmed() throws Exception {
    assertU(adoc("id", "1", "content_txt_en", "1 - ambulance medic"));
    assertU(adoc("id", "2", "content_txt_en", "2 - medical hospital"));
    assertU(adoc("id", "3", "content_txt_en", "3 - ambulance medical treatment"));
    assertU(adoc("id", "4", "content_txt_en", "4 - ambulance medicale treatment"));
    assertU(commit());

    // Stemmed wildcards are dependant on the schema.
    // If only the stemmed token is indexed, wildcards longer than the stem will never match.
    assertResults("content_txt_en:\"ambulance medica*\"", 1, "4");
    assertResults("content_txt_en:medica*", "lucene", 1, "4");
    assertResults("content_txt_en:medica*", "edismax", 1, "4");
  }
  
  @Test @Ignore
  public void testWildcardPhrasedHighlighting() throws Exception {
    assertU(adoc("id", "1", "content_txt", "1 - aaabar baraaa of bbb ccc"));
    assertU(adoc("id", "1a", "content_txt", "1a - aaabar of bbb baraaa ccc"));
    assertU(adoc("id", "2", "content_txt", "2 - baa bbb bcc"));
    assertU(adoc("id", "3", "content_txt", "3 - aaafoo fooaaa caa cbb ccc foo bar baz fooo barr bazz"));
    assertU(adoc("id", "4", "content_txt", "4 - daa dbb the dcc"));
    assertU(adoc("id", "5", "content_txt", "5 - caa aaa ccc bbb daa dbb caa cbb aaa ccc"));
    assertU(adoc("id", "6", "content_txt", "6 - foo bar aaa"));
    assertU(adoc("id", "7", "content_txt", "7 - bar aaa foo bar baz bbb baz"));
    assertU(commit());
    
    final String[] params = new String[]{
        "df", "content_txt",
        "indent", "true",
        "sort", "score desc, id asc",
        "q.op", "AND",
        "debugQuery", "true",
        "fl", "*,score", 
        "hl", "true"
    };

    assertResults(params, "\"bar ba*\" ccc", 3, "1", "8", "12");
    assertResults(params, "bar", 3, "1", "8", "12");
//    assertResults("content_txt_en:medica*", "lucene", 3, "1", "8", "12");
    assertResults("content_txt_en:medica*", "edismax", 3, "1", "8", "12");
    
    // synonym highlighting
    assertResults(params, "fooaaa", 3, "1", "1a", "3");
  }
  
}
