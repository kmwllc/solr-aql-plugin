parser grammar AsqlParser;

options { tokenVocab=AsqlLexer; }

// A single query string
queryString   : (WS)* term (WS)* ;

// A single term or expression
term    
              : NOT term                                         #notOp
              | term AND term                                    #andOp
              | term IMPLIED term                                #impliedOp
              | term OR term                                     #orOp
              | wordOrPhrase                                     #wordOrPhraseOp
              | LPAREN term RPAREN                               #groupedTermOp
              ;


// A word or phrase with optional field and range syntax
wordOrPhrase  
              : (field=WORD COLON range=RANGE?)? phrase=PHRASE    #phraseOp
              | (field=WORD COLON range=RANGE?)? word=WORD        #wordOp
              ;
              
