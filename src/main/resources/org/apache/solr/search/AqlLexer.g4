lexer grammar AqlLexer;

WORD    : '*'
        | ~[ \t　\r\n"()[\]:=&|><!,]+
        ;
        
PHRASE  : '"' ( '\\"' | . )+? '"' ;

ANDP    : [aA][nN][dD]'(' WS?;
ORP     : [oO][rR]'(' WS?;
NOTP    : [nN][oO][tT]'(' WS?;
QUERYP  : [qQ][uU][eE][rR][yY]'(' WS?;

RPAREN  : WS? ')';
COMMA   : WS? ',' WS?;
COLON   : ':';
RANGE     : '<' | '>' | '<=' | '>=';
EQ      : WS? '=' WS?;

WS      : [ \t　\r\n\u200B\uFEFF]+ ;
