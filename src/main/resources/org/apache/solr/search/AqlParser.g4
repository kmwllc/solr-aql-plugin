parser grammar AqlParser;

options { tokenVocab=AqlLexer; }

// A single query string - one line
queryString : (WS)? term (WS)? EOF ;

// A single term or expression - word, phrase, AND, OR, NOT, etc
term    
        : ANDP term (COMMA term)* (COMMA parameter)*              #unmatchedParen
        | ANDP term (COMMA term)* (COMMA parameter)* RPAREN       #andOp
        | ORP term (COMMA term)* (COMMA parameter)*               #unmatchedParen
        | ORP term (COMMA term)* (COMMA parameter)* RPAREN        #orOp
        | NOTP term (COMMA parameter)*                            #unmatchedParen
        | NOTP term (COMMA parameter)* RPAREN                     #notOp
        | QUERYP PHRASE (COMMA parameter)*                        #unmatchedParen
        | QUERYP PHRASE (COMMA parameter)* RPAREN                 #embeddedQueryOp
        | wordOrPhrase (WS wordOrPhrase)+                         #doubleWordError
        | wordOrPhrase                                            #wordOrPhraseOp
        ;


wordOrPhrase  : (field=WORD COLON range=RANGE?)? phrase=PHRASE         #phraseOp
              | (field=WORD COLON range=RANGE?)? word=WORD             {!$word.text.equalsIgnoreCase("AND(") 
                                                        && !$word.text.equalsIgnoreCase("OR(") 
                                                        && !$word.text.equalsIgnoreCase("NOT(") 
                                                        && !$word.text.equalsIgnoreCase("JOIN") 
                                                        && !$word.text.equalsIgnoreCase("NEAR")}? #wordOp
              ;
                                                       
parameter     : paramName=WORD EQ paramValue=WORD 
              ;

