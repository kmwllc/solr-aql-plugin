lexer grammar AsqlLexer;

WORD      : '*'
          | ~[ \t　\r\n"()[\]:=><]+
          ;
        
PHRASE    : '-'? '"' ( '\\"' | . )+? '"' ;

COLON     : ':';
RANGE     : '<' | '>' | '<=' | '>=';
LPAREN    : '(';
RPAREN    : ')';

OR      : WS 'OR' WS
        ;
AND     : WS 'AND' WS
        ;
IMPLIED : WS
        ;
NOT     : 'NOT' WS;

WS        : [ \t　\r\n\u200B\uFEFF]+;
CR        : WS* [\r\n]+ -> skip ;
EOF_RULE  : EOF -> skip;
