// Generated from org/apache/solr/search/AqlLexer.g4 by ANTLR 4.13.2
package org.apache.solr.search;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue", "this-escape"})
public class AqlLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.13.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WORD=1, PHRASE=2, ANDP=3, ORP=4, NOTP=5, QUERYP=6, RPAREN=7, COMMA=8, 
		COLON=9, RANGE=10, EQ=11, WS=12;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"WORD", "PHRASE", "ANDP", "ORP", "NOTP", "QUERYP", "RPAREN", "COMMA", 
			"COLON", "RANGE", "EQ", "WS"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "':'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "WORD", "PHRASE", "ANDP", "ORP", "NOTP", "QUERYP", "RPAREN", "COMMA", 
			"COLON", "RANGE", "EQ", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public AqlLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "AqlLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\u0004\u0000\fi\u0006\uffff\uffff\u0002\u0000\u0007\u0000\u0002\u0001"+
		"\u0007\u0001\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004"+
		"\u0007\u0004\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007"+
		"\u0007\u0007\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b"+
		"\u0007\u000b\u0001\u0000\u0001\u0000\u0004\u0000\u001c\b\u0000\u000b\u0000"+
		"\f\u0000\u001d\u0003\u0000 \b\u0000\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0004\u0001&\b\u0001\u000b\u0001\f\u0001\'\u0001\u0001\u0001"+
		"\u0001\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0003"+
		"\u00021\b\u0002\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0003"+
		"\u00037\b\u0003\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001"+
		"\u0004\u0003\u0004>\b\u0004\u0001\u0005\u0001\u0005\u0001\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0005\u0003\u0005G\b\u0005\u0001"+
		"\u0006\u0003\u0006J\b\u0006\u0001\u0006\u0001\u0006\u0001\u0007\u0003"+
		"\u0007O\b\u0007\u0001\u0007\u0001\u0007\u0003\u0007S\b\u0007\u0001\b\u0001"+
		"\b\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0003\t\\\b\t\u0001\n\u0003"+
		"\n_\b\n\u0001\n\u0001\n\u0003\nc\b\n\u0001\u000b\u0004\u000bf\b\u000b"+
		"\u000b\u000b\f\u000bg\u0001\'\u0000\f\u0001\u0001\u0003\u0002\u0005\u0003"+
		"\u0007\u0004\t\u0005\u000b\u0006\r\u0007\u000f\b\u0011\t\u0013\n\u0015"+
		"\u000b\u0017\f\u0001\u0000\r\f\u0000\t\n\r\r \"&&(),,::<>[[]]||\u3000"+
		"\u3000\u0002\u0000AAaa\u0002\u0000NNnn\u0002\u0000DDdd\u0002\u0000OOo"+
		"o\u0002\u0000RRrr\u0002\u0000TTtt\u0002\u0000QQqq\u0002\u0000UUuu\u0002"+
		"\u0000EEee\u0002\u0000YYyy\u0002\u0000<<>>\u0006\u0000\t\n\r\r  \u200b"+
		"\u200b\u3000\u3000\u8000\ufeff\u8000\ufeffx\u0000\u0001\u0001\u0000\u0000"+
		"\u0000\u0000\u0003\u0001\u0000\u0000\u0000\u0000\u0005\u0001\u0000\u0000"+
		"\u0000\u0000\u0007\u0001\u0000\u0000\u0000\u0000\t\u0001\u0000\u0000\u0000"+
		"\u0000\u000b\u0001\u0000\u0000\u0000\u0000\r\u0001\u0000\u0000\u0000\u0000"+
		"\u000f\u0001\u0000\u0000\u0000\u0000\u0011\u0001\u0000\u0000\u0000\u0000"+
		"\u0013\u0001\u0000\u0000\u0000\u0000\u0015\u0001\u0000\u0000\u0000\u0000"+
		"\u0017\u0001\u0000\u0000\u0000\u0001\u001f\u0001\u0000\u0000\u0000\u0003"+
		"!\u0001\u0000\u0000\u0000\u0005+\u0001\u0000\u0000\u0000\u00072\u0001"+
		"\u0000\u0000\u0000\t8\u0001\u0000\u0000\u0000\u000b?\u0001\u0000\u0000"+
		"\u0000\rI\u0001\u0000\u0000\u0000\u000fN\u0001\u0000\u0000\u0000\u0011"+
		"T\u0001\u0000\u0000\u0000\u0013[\u0001\u0000\u0000\u0000\u0015^\u0001"+
		"\u0000\u0000\u0000\u0017e\u0001\u0000\u0000\u0000\u0019 \u0005*\u0000"+
		"\u0000\u001a\u001c\b\u0000\u0000\u0000\u001b\u001a\u0001\u0000\u0000\u0000"+
		"\u001c\u001d\u0001\u0000\u0000\u0000\u001d\u001b\u0001\u0000\u0000\u0000"+
		"\u001d\u001e\u0001\u0000\u0000\u0000\u001e \u0001\u0000\u0000\u0000\u001f"+
		"\u0019\u0001\u0000\u0000\u0000\u001f\u001b\u0001\u0000\u0000\u0000 \u0002"+
		"\u0001\u0000\u0000\u0000!%\u0005\"\u0000\u0000\"#\u0005\\\u0000\u0000"+
		"#&\u0005\"\u0000\u0000$&\t\u0000\u0000\u0000%\"\u0001\u0000\u0000\u0000"+
		"%$\u0001\u0000\u0000\u0000&\'\u0001\u0000\u0000\u0000\'(\u0001\u0000\u0000"+
		"\u0000\'%\u0001\u0000\u0000\u0000()\u0001\u0000\u0000\u0000)*\u0005\""+
		"\u0000\u0000*\u0004\u0001\u0000\u0000\u0000+,\u0007\u0001\u0000\u0000"+
		",-\u0007\u0002\u0000\u0000-.\u0007\u0003\u0000\u0000.0\u0005(\u0000\u0000"+
		"/1\u0003\u0017\u000b\u00000/\u0001\u0000\u0000\u000001\u0001\u0000\u0000"+
		"\u00001\u0006\u0001\u0000\u0000\u000023\u0007\u0004\u0000\u000034\u0007"+
		"\u0005\u0000\u000046\u0005(\u0000\u000057\u0003\u0017\u000b\u000065\u0001"+
		"\u0000\u0000\u000067\u0001\u0000\u0000\u00007\b\u0001\u0000\u0000\u0000"+
		"89\u0007\u0002\u0000\u00009:\u0007\u0004\u0000\u0000:;\u0007\u0006\u0000"+
		"\u0000;=\u0005(\u0000\u0000<>\u0003\u0017\u000b\u0000=<\u0001\u0000\u0000"+
		"\u0000=>\u0001\u0000\u0000\u0000>\n\u0001\u0000\u0000\u0000?@\u0007\u0007"+
		"\u0000\u0000@A\u0007\b\u0000\u0000AB\u0007\t\u0000\u0000BC\u0007\u0005"+
		"\u0000\u0000CD\u0007\n\u0000\u0000DF\u0005(\u0000\u0000EG\u0003\u0017"+
		"\u000b\u0000FE\u0001\u0000\u0000\u0000FG\u0001\u0000\u0000\u0000G\f\u0001"+
		"\u0000\u0000\u0000HJ\u0003\u0017\u000b\u0000IH\u0001\u0000\u0000\u0000"+
		"IJ\u0001\u0000\u0000\u0000JK\u0001\u0000\u0000\u0000KL\u0005)\u0000\u0000"+
		"L\u000e\u0001\u0000\u0000\u0000MO\u0003\u0017\u000b\u0000NM\u0001\u0000"+
		"\u0000\u0000NO\u0001\u0000\u0000\u0000OP\u0001\u0000\u0000\u0000PR\u0005"+
		",\u0000\u0000QS\u0003\u0017\u000b\u0000RQ\u0001\u0000\u0000\u0000RS\u0001"+
		"\u0000\u0000\u0000S\u0010\u0001\u0000\u0000\u0000TU\u0005:\u0000\u0000"+
		"U\u0012\u0001\u0000\u0000\u0000V\\\u0007\u000b\u0000\u0000WX\u0005<\u0000"+
		"\u0000X\\\u0005=\u0000\u0000YZ\u0005>\u0000\u0000Z\\\u0005=\u0000\u0000"+
		"[V\u0001\u0000\u0000\u0000[W\u0001\u0000\u0000\u0000[Y\u0001\u0000\u0000"+
		"\u0000\\\u0014\u0001\u0000\u0000\u0000]_\u0003\u0017\u000b\u0000^]\u0001"+
		"\u0000\u0000\u0000^_\u0001\u0000\u0000\u0000_`\u0001\u0000\u0000\u0000"+
		"`b\u0005=\u0000\u0000ac\u0003\u0017\u000b\u0000ba\u0001\u0000\u0000\u0000"+
		"bc\u0001\u0000\u0000\u0000c\u0016\u0001\u0000\u0000\u0000df\u0007\f\u0000"+
		"\u0000ed\u0001\u0000\u0000\u0000fg\u0001\u0000\u0000\u0000ge\u0001\u0000"+
		"\u0000\u0000gh\u0001\u0000\u0000\u0000h\u0018\u0001\u0000\u0000\u0000"+
		"\u0010\u0000\u001d\u001f%\'06=FINR[^bg\u0000";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}