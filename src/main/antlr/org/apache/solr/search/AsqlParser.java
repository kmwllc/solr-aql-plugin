// Generated from org/apache/solr/search/AsqlParser.g4 by ANTLR 4.13.2
package org.apache.solr.search;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue", "this-escape"})
public class AsqlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.13.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WORD=1, PHRASE=2, COLON=3, RANGE=4, LPAREN=5, RPAREN=6, OR=7, AND=8, IMPLIED=9, 
		NOT=10, WS=11, CR=12, EOF_RULE=13;
	public static final int
		RULE_queryString = 0, RULE_term = 1, RULE_wordOrPhrase = 2;
	private static String[] makeRuleNames() {
		return new String[] {
			"queryString", "term", "wordOrPhrase"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "':'", null, "'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "WORD", "PHRASE", "COLON", "RANGE", "LPAREN", "RPAREN", "OR", "AND", 
			"IMPLIED", "NOT", "WS", "CR", "EOF_RULE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "AsqlParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AsqlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class QueryStringContext extends ParserRuleContext {
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public List<TerminalNode> WS() { return getTokens(AsqlParser.WS); }
		public TerminalNode WS(int i) {
			return getToken(AsqlParser.WS, i);
		}
		public QueryStringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryString; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitQueryString(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryStringContext queryString() throws RecognitionException {
		QueryStringContext _localctx = new QueryStringContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_queryString);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(9);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==WS) {
				{
				{
				setState(6);
				match(WS);
				}
				}
				setState(11);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(12);
			term(0);
			setState(16);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==WS) {
				{
				{
				setState(13);
				match(WS);
				}
				}
				setState(18);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TermContext extends ParserRuleContext {
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	 
		public TermContext() { }
		public void copyFrom(TermContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class OrOpContext extends TermContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public TerminalNode OR() { return getToken(AsqlParser.OR, 0); }
		public OrOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitOrOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class AndOpContext extends TermContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public TerminalNode AND() { return getToken(AsqlParser.AND, 0); }
		public AndOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitAndOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class GroupedTermOpContext extends TermContext {
		public TerminalNode LPAREN() { return getToken(AsqlParser.LPAREN, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AsqlParser.RPAREN, 0); }
		public GroupedTermOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitGroupedTermOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NotOpContext extends TermContext {
		public TerminalNode NOT() { return getToken(AsqlParser.NOT, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public NotOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitNotOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class ImpliedOpContext extends TermContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public TerminalNode IMPLIED() { return getToken(AsqlParser.IMPLIED, 0); }
		public ImpliedOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitImpliedOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class WordOrPhraseOpContext extends TermContext {
		public WordOrPhraseContext wordOrPhrase() {
			return getRuleContext(WordOrPhraseContext.class,0);
		}
		public WordOrPhraseOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitWordOrPhraseOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		return term(0);
	}

	private TermContext term(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TermContext _localctx = new TermContext(_ctx, _parentState);
		TermContext _prevctx = _localctx;
		int _startState = 2;
		enterRecursionRule(_localctx, 2, RULE_term, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(27);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NOT:
				{
				_localctx = new NotOpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(20);
				match(NOT);
				setState(21);
				term(6);
				}
				break;
			case WORD:
			case PHRASE:
				{
				_localctx = new WordOrPhraseOpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(22);
				wordOrPhrase();
				}
				break;
			case LPAREN:
				{
				_localctx = new GroupedTermOpContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(23);
				match(LPAREN);
				setState(24);
				term(0);
				setState(25);
				match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(40);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(38);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
					case 1:
						{
						_localctx = new AndOpContext(new TermContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(29);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(30);
						match(AND);
						setState(31);
						term(6);
						}
						break;
					case 2:
						{
						_localctx = new ImpliedOpContext(new TermContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(32);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(33);
						match(IMPLIED);
						setState(34);
						term(5);
						}
						break;
					case 3:
						{
						_localctx = new OrOpContext(new TermContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_term);
						setState(35);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(36);
						match(OR);
						setState(37);
						term(4);
						}
						break;
					}
					} 
				}
				setState(42);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class WordOrPhraseContext extends ParserRuleContext {
		public WordOrPhraseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wordOrPhrase; }
	 
		public WordOrPhraseContext() { }
		public void copyFrom(WordOrPhraseContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class WordOpContext extends WordOrPhraseContext {
		public Token field;
		public Token range;
		public Token word;
		public List<TerminalNode> WORD() { return getTokens(AsqlParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(AsqlParser.WORD, i);
		}
		public TerminalNode COLON() { return getToken(AsqlParser.COLON, 0); }
		public TerminalNode RANGE() { return getToken(AsqlParser.RANGE, 0); }
		public WordOpContext(WordOrPhraseContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitWordOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PhraseOpContext extends WordOrPhraseContext {
		public Token field;
		public Token range;
		public Token phrase;
		public TerminalNode PHRASE() { return getToken(AsqlParser.PHRASE, 0); }
		public TerminalNode COLON() { return getToken(AsqlParser.COLON, 0); }
		public TerminalNode WORD() { return getToken(AsqlParser.WORD, 0); }
		public TerminalNode RANGE() { return getToken(AsqlParser.RANGE, 0); }
		public PhraseOpContext(WordOrPhraseContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AsqlParserVisitor ) return ((AsqlParserVisitor<? extends T>)visitor).visitPhraseOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WordOrPhraseContext wordOrPhrase() throws RecognitionException {
		WordOrPhraseContext _localctx = new WordOrPhraseContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_wordOrPhrase);
		int _la;
		try {
			setState(59);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new PhraseOpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(48);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==WORD) {
					{
					setState(43);
					((PhraseOpContext)_localctx).field = match(WORD);
					setState(44);
					match(COLON);
					setState(46);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==RANGE) {
						{
						setState(45);
						((PhraseOpContext)_localctx).range = match(RANGE);
						}
					}

					}
				}

				setState(50);
				((PhraseOpContext)_localctx).phrase = match(PHRASE);
				}
				break;
			case 2:
				_localctx = new WordOpContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(56);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
				case 1:
					{
					setState(51);
					((WordOpContext)_localctx).field = match(WORD);
					setState(52);
					match(COLON);
					setState(54);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==RANGE) {
						{
						setState(53);
						((WordOpContext)_localctx).range = match(RANGE);
						}
					}

					}
					break;
				}
				setState(58);
				((WordOpContext)_localctx).word = match(WORD);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 1:
			return term_sempred((TermContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean term_sempred(TermContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 5);
		case 1:
			return precpred(_ctx, 4);
		case 2:
			return precpred(_ctx, 3);
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001\r>\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0001\u0000\u0005\u0000\b\b\u0000\n\u0000\f\u0000\u000b"+
		"\t\u0000\u0001\u0000\u0001\u0000\u0005\u0000\u000f\b\u0000\n\u0000\f\u0000"+
		"\u0012\t\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0003\u0001\u001c\b\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0005\u0001\'\b\u0001\n\u0001\f\u0001*\t\u0001"+
		"\u0001\u0002\u0001\u0002\u0001\u0002\u0003\u0002/\b\u0002\u0003\u0002"+
		"1\b\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0003\u0002"+
		"7\b\u0002\u0003\u00029\b\u0002\u0001\u0002\u0003\u0002<\b\u0002\u0001"+
		"\u0002\u0000\u0001\u0002\u0003\u0000\u0002\u0004\u0000\u0000F\u0000\t"+
		"\u0001\u0000\u0000\u0000\u0002\u001b\u0001\u0000\u0000\u0000\u0004;\u0001"+
		"\u0000\u0000\u0000\u0006\b\u0005\u000b\u0000\u0000\u0007\u0006\u0001\u0000"+
		"\u0000\u0000\b\u000b\u0001\u0000\u0000\u0000\t\u0007\u0001\u0000\u0000"+
		"\u0000\t\n\u0001\u0000\u0000\u0000\n\f\u0001\u0000\u0000\u0000\u000b\t"+
		"\u0001\u0000\u0000\u0000\f\u0010\u0003\u0002\u0001\u0000\r\u000f\u0005"+
		"\u000b\u0000\u0000\u000e\r\u0001\u0000\u0000\u0000\u000f\u0012\u0001\u0000"+
		"\u0000\u0000\u0010\u000e\u0001\u0000\u0000\u0000\u0010\u0011\u0001\u0000"+
		"\u0000\u0000\u0011\u0001\u0001\u0000\u0000\u0000\u0012\u0010\u0001\u0000"+
		"\u0000\u0000\u0013\u0014\u0006\u0001\uffff\uffff\u0000\u0014\u0015\u0005"+
		"\n\u0000\u0000\u0015\u001c\u0003\u0002\u0001\u0006\u0016\u001c\u0003\u0004"+
		"\u0002\u0000\u0017\u0018\u0005\u0005\u0000\u0000\u0018\u0019\u0003\u0002"+
		"\u0001\u0000\u0019\u001a\u0005\u0006\u0000\u0000\u001a\u001c\u0001\u0000"+
		"\u0000\u0000\u001b\u0013\u0001\u0000\u0000\u0000\u001b\u0016\u0001\u0000"+
		"\u0000\u0000\u001b\u0017\u0001\u0000\u0000\u0000\u001c(\u0001\u0000\u0000"+
		"\u0000\u001d\u001e\n\u0005\u0000\u0000\u001e\u001f\u0005\b\u0000\u0000"+
		"\u001f\'\u0003\u0002\u0001\u0006 !\n\u0004\u0000\u0000!\"\u0005\t\u0000"+
		"\u0000\"\'\u0003\u0002\u0001\u0005#$\n\u0003\u0000\u0000$%\u0005\u0007"+
		"\u0000\u0000%\'\u0003\u0002\u0001\u0004&\u001d\u0001\u0000\u0000\u0000"+
		"& \u0001\u0000\u0000\u0000&#\u0001\u0000\u0000\u0000\'*\u0001\u0000\u0000"+
		"\u0000(&\u0001\u0000\u0000\u0000()\u0001\u0000\u0000\u0000)\u0003\u0001"+
		"\u0000\u0000\u0000*(\u0001\u0000\u0000\u0000+,\u0005\u0001\u0000\u0000"+
		",.\u0005\u0003\u0000\u0000-/\u0005\u0004\u0000\u0000.-\u0001\u0000\u0000"+
		"\u0000./\u0001\u0000\u0000\u0000/1\u0001\u0000\u0000\u00000+\u0001\u0000"+
		"\u0000\u000001\u0001\u0000\u0000\u000012\u0001\u0000\u0000\u00002<\u0005"+
		"\u0002\u0000\u000034\u0005\u0001\u0000\u000046\u0005\u0003\u0000\u0000"+
		"57\u0005\u0004\u0000\u000065\u0001\u0000\u0000\u000067\u0001\u0000\u0000"+
		"\u000079\u0001\u0000\u0000\u000083\u0001\u0000\u0000\u000089\u0001\u0000"+
		"\u0000\u00009:\u0001\u0000\u0000\u0000:<\u0005\u0001\u0000\u0000;0\u0001"+
		"\u0000\u0000\u0000;8\u0001\u0000\u0000\u0000<\u0005\u0001\u0000\u0000"+
		"\u0000\n\t\u0010\u001b&(.068;";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}