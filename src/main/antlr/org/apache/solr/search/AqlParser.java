// Generated from org/apache/solr/search/AqlParser.g4 by ANTLR 4.13.2
package org.apache.solr.search;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue", "this-escape"})
public class AqlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.13.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WORD=1, PHRASE=2, ANDP=3, ORP=4, NOTP=5, QUERYP=6, RPAREN=7, COMMA=8, 
		COLON=9, RANGE=10, EQ=11, WS=12;
	public static final int
		RULE_queryString = 0, RULE_term = 1, RULE_wordOrPhrase = 2, RULE_parameter = 3;
	private static String[] makeRuleNames() {
		return new String[] {
			"queryString", "term", "wordOrPhrase", "parameter"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, "':'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "WORD", "PHRASE", "ANDP", "ORP", "NOTP", "QUERYP", "RPAREN", "COMMA", 
			"COLON", "RANGE", "EQ", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "AqlParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AqlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@SuppressWarnings("CheckReturnValue")
	public static class QueryStringContext extends ParserRuleContext {
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode EOF() { return getToken(AqlParser.EOF, 0); }
		public List<TerminalNode> WS() { return getTokens(AqlParser.WS); }
		public TerminalNode WS(int i) {
			return getToken(AqlParser.WS, i);
		}
		public QueryStringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_queryString; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitQueryString(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryStringContext queryString() throws RecognitionException {
		QueryStringContext _localctx = new QueryStringContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_queryString);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(9);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WS) {
				{
				setState(8);
				match(WS);
				}
			}

			setState(11);
			term();
			setState(13);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==WS) {
				{
				setState(12);
				match(WS);
				}
			}

			setState(15);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class TermContext extends ParserRuleContext {
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
	 
		public TermContext() { }
		public void copyFrom(TermContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class OrOpContext extends TermContext {
		public TerminalNode ORP() { return getToken(AqlParser.ORP, 0); }
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public TerminalNode RPAREN() { return getToken(AqlParser.RPAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AqlParser.COMMA, i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public OrOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitOrOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class EmbeddedQueryOpContext extends TermContext {
		public TerminalNode QUERYP() { return getToken(AqlParser.QUERYP, 0); }
		public TerminalNode PHRASE() { return getToken(AqlParser.PHRASE, 0); }
		public TerminalNode RPAREN() { return getToken(AqlParser.RPAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AqlParser.COMMA, i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public EmbeddedQueryOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitEmbeddedQueryOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class AndOpContext extends TermContext {
		public TerminalNode ANDP() { return getToken(AqlParser.ANDP, 0); }
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public TerminalNode RPAREN() { return getToken(AqlParser.RPAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AqlParser.COMMA, i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public AndOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitAndOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class UnmatchedParenContext extends TermContext {
		public TerminalNode ANDP() { return getToken(AqlParser.ANDP, 0); }
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AqlParser.COMMA, i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public TerminalNode ORP() { return getToken(AqlParser.ORP, 0); }
		public TerminalNode NOTP() { return getToken(AqlParser.NOTP, 0); }
		public TerminalNode QUERYP() { return getToken(AqlParser.QUERYP, 0); }
		public TerminalNode PHRASE() { return getToken(AqlParser.PHRASE, 0); }
		public UnmatchedParenContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitUnmatchedParen(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class DoubleWordErrorContext extends TermContext {
		public List<WordOrPhraseContext> wordOrPhrase() {
			return getRuleContexts(WordOrPhraseContext.class);
		}
		public WordOrPhraseContext wordOrPhrase(int i) {
			return getRuleContext(WordOrPhraseContext.class,i);
		}
		public List<TerminalNode> WS() { return getTokens(AqlParser.WS); }
		public TerminalNode WS(int i) {
			return getToken(AqlParser.WS, i);
		}
		public DoubleWordErrorContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitDoubleWordError(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class NotOpContext extends TermContext {
		public TerminalNode NOTP() { return getToken(AqlParser.NOTP, 0); }
		public TermContext term() {
			return getRuleContext(TermContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AqlParser.RPAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AqlParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AqlParser.COMMA, i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public NotOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitNotOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class WordOrPhraseOpContext extends TermContext {
		public WordOrPhraseContext wordOrPhrase() {
			return getRuleContext(WordOrPhraseContext.class,0);
		}
		public WordOrPhraseOpContext(TermContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitWordOrPhraseOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_term);
		int _la;
		try {
			int _alt;
			setState(132);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				_localctx = new UnmatchedParenContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(17);
				match(ANDP);
				setState(18);
				term();
				setState(23);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(19);
						match(COMMA);
						setState(20);
						term();
						}
						} 
					}
					setState(25);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
				}
				setState(30);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(26);
						match(COMMA);
						setState(27);
						parameter();
						}
						} 
					}
					setState(32);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
				}
				}
				break;
			case 2:
				_localctx = new AndOpContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(33);
				match(ANDP);
				setState(34);
				term();
				setState(39);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(35);
						match(COMMA);
						setState(36);
						term();
						}
						} 
					}
					setState(41);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
				}
				setState(46);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(42);
					match(COMMA);
					setState(43);
					parameter();
					}
					}
					setState(48);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(49);
				match(RPAREN);
				}
				break;
			case 3:
				_localctx = new UnmatchedParenContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(51);
				match(ORP);
				setState(52);
				term();
				setState(57);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(53);
						match(COMMA);
						setState(54);
						term();
						}
						} 
					}
					setState(59);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				}
				setState(64);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(60);
						match(COMMA);
						setState(61);
						parameter();
						}
						} 
					}
					setState(66);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,7,_ctx);
				}
				}
				break;
			case 4:
				_localctx = new OrOpContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(67);
				match(ORP);
				setState(68);
				term();
				setState(73);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(69);
						match(COMMA);
						setState(70);
						term();
						}
						} 
					}
					setState(75);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				}
				setState(80);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(76);
					match(COMMA);
					setState(77);
					parameter();
					}
					}
					setState(82);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(83);
				match(RPAREN);
				}
				break;
			case 5:
				_localctx = new UnmatchedParenContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(85);
				match(NOTP);
				setState(86);
				term();
				setState(91);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(87);
						match(COMMA);
						setState(88);
						parameter();
						}
						} 
					}
					setState(93);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				}
				}
				break;
			case 6:
				_localctx = new NotOpContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(94);
				match(NOTP);
				setState(95);
				term();
				setState(100);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(96);
					match(COMMA);
					setState(97);
					parameter();
					}
					}
					setState(102);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(103);
				match(RPAREN);
				}
				break;
			case 7:
				_localctx = new UnmatchedParenContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(105);
				match(QUERYP);
				setState(106);
				match(PHRASE);
				setState(111);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(107);
						match(COMMA);
						setState(108);
						parameter();
						}
						} 
					}
					setState(113);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
				}
				}
				break;
			case 8:
				_localctx = new EmbeddedQueryOpContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(114);
				match(QUERYP);
				setState(115);
				match(PHRASE);
				setState(120);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(116);
					match(COMMA);
					setState(117);
					parameter();
					}
					}
					setState(122);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(123);
				match(RPAREN);
				}
				break;
			case 9:
				_localctx = new DoubleWordErrorContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(124);
				wordOrPhrase();
				setState(127); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(125);
						match(WS);
						setState(126);
						wordOrPhrase();
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(129); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case 10:
				_localctx = new WordOrPhraseOpContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(131);
				wordOrPhrase();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class WordOrPhraseContext extends ParserRuleContext {
		public WordOrPhraseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_wordOrPhrase; }
	 
		public WordOrPhraseContext() { }
		public void copyFrom(WordOrPhraseContext ctx) {
			super.copyFrom(ctx);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class WordOpContext extends WordOrPhraseContext {
		public Token field;
		public Token range;
		public Token word;
		public List<TerminalNode> WORD() { return getTokens(AqlParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(AqlParser.WORD, i);
		}
		public TerminalNode COLON() { return getToken(AqlParser.COLON, 0); }
		public TerminalNode RANGE() { return getToken(AqlParser.RANGE, 0); }
		public WordOpContext(WordOrPhraseContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitWordOp(this);
			else return visitor.visitChildren(this);
		}
	}
	@SuppressWarnings("CheckReturnValue")
	public static class PhraseOpContext extends WordOrPhraseContext {
		public Token field;
		public Token range;
		public Token phrase;
		public TerminalNode PHRASE() { return getToken(AqlParser.PHRASE, 0); }
		public TerminalNode COLON() { return getToken(AqlParser.COLON, 0); }
		public TerminalNode WORD() { return getToken(AqlParser.WORD, 0); }
		public TerminalNode RANGE() { return getToken(AqlParser.RANGE, 0); }
		public PhraseOpContext(WordOrPhraseContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitPhraseOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WordOrPhraseContext wordOrPhrase() throws RecognitionException {
		WordOrPhraseContext _localctx = new WordOrPhraseContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_wordOrPhrase);
		int _la;
		try {
			setState(151);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				_localctx = new PhraseOpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(139);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==WORD) {
					{
					setState(134);
					((PhraseOpContext)_localctx).field = match(WORD);
					setState(135);
					match(COLON);
					setState(137);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==RANGE) {
						{
						setState(136);
						((PhraseOpContext)_localctx).range = match(RANGE);
						}
					}

					}
				}

				setState(141);
				((PhraseOpContext)_localctx).phrase = match(PHRASE);
				}
				break;
			case 2:
				_localctx = new WordOpContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(147);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,19,_ctx) ) {
				case 1:
					{
					setState(142);
					((WordOpContext)_localctx).field = match(WORD);
					setState(143);
					match(COLON);
					setState(145);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==RANGE) {
						{
						setState(144);
						((WordOpContext)_localctx).range = match(RANGE);
						}
					}

					}
					break;
				}
				setState(149);
				((WordOpContext)_localctx).word = match(WORD);
				setState(150);
				if (!(!(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("AND(") 
				                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("OR(") 
				                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("NOT(") 
				                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("JOIN") 
				                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("NEAR"))) throw new FailedPredicateException(this, "!$word.text.equalsIgnoreCase(\"AND(\") \n                                                        && !$word.text.equalsIgnoreCase(\"OR(\") \n                                                        && !$word.text.equalsIgnoreCase(\"NOT(\") \n                                                        && !$word.text.equalsIgnoreCase(\"JOIN\") \n                                                        && !$word.text.equalsIgnoreCase(\"NEAR\")");
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	@SuppressWarnings("CheckReturnValue")
	public static class ParameterContext extends ParserRuleContext {
		public Token paramName;
		public Token paramValue;
		public TerminalNode EQ() { return getToken(AqlParser.EQ, 0); }
		public List<TerminalNode> WORD() { return getTokens(AqlParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(AqlParser.WORD, i);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AqlParserVisitor ) return ((AqlParserVisitor<? extends T>)visitor).visitParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(153);
			((ParameterContext)_localctx).paramName = match(WORD);
			setState(154);
			match(EQ);
			setState(155);
			((ParameterContext)_localctx).paramValue = match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2:
			return wordOrPhrase_sempred((WordOrPhraseContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean wordOrPhrase_sempred(WordOrPhraseContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("AND(") 
		                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("OR(") 
		                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("NOT(") 
		                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("JOIN") 
		                                                        && !(((WordOpContext)_localctx).word!=null?((WordOpContext)_localctx).word.getText():null).equalsIgnoreCase("NEAR");
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001\f\u009e\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0001\u0000\u0003\u0000\n\b"+
		"\u0000\u0001\u0000\u0001\u0000\u0003\u0000\u000e\b\u0000\u0001\u0000\u0001"+
		"\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0005\u0001\u0016"+
		"\b\u0001\n\u0001\f\u0001\u0019\t\u0001\u0001\u0001\u0001\u0001\u0005\u0001"+
		"\u001d\b\u0001\n\u0001\f\u0001 \t\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0005\u0001&\b\u0001\n\u0001\f\u0001)\t\u0001\u0001"+
		"\u0001\u0001\u0001\u0005\u0001-\b\u0001\n\u0001\f\u00010\t\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0005"+
		"\u00018\b\u0001\n\u0001\f\u0001;\t\u0001\u0001\u0001\u0001\u0001\u0005"+
		"\u0001?\b\u0001\n\u0001\f\u0001B\t\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0005\u0001H\b\u0001\n\u0001\f\u0001K\t\u0001\u0001"+
		"\u0001\u0001\u0001\u0005\u0001O\b\u0001\n\u0001\f\u0001R\t\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0005"+
		"\u0001Z\b\u0001\n\u0001\f\u0001]\t\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0005\u0001c\b\u0001\n\u0001\f\u0001f\t\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0005"+
		"\u0001n\b\u0001\n\u0001\f\u0001q\t\u0001\u0001\u0001\u0001\u0001\u0001"+
		"\u0001\u0001\u0001\u0005\u0001w\b\u0001\n\u0001\f\u0001z\t\u0001\u0001"+
		"\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0004\u0001\u0080\b\u0001\u000b"+
		"\u0001\f\u0001\u0081\u0001\u0001\u0003\u0001\u0085\b\u0001\u0001\u0002"+
		"\u0001\u0002\u0001\u0002\u0003\u0002\u008a\b\u0002\u0003\u0002\u008c\b"+
		"\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0001\u0002\u0003\u0002\u0092"+
		"\b\u0002\u0003\u0002\u0094\b\u0002\u0001\u0002\u0001\u0002\u0003\u0002"+
		"\u0098\b\u0002\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003"+
		"\u0000\u0000\u0004\u0000\u0002\u0004\u0006\u0000\u0000\u00b6\u0000\t\u0001"+
		"\u0000\u0000\u0000\u0002\u0084\u0001\u0000\u0000\u0000\u0004\u0097\u0001"+
		"\u0000\u0000\u0000\u0006\u0099\u0001\u0000\u0000\u0000\b\n\u0005\f\u0000"+
		"\u0000\t\b\u0001\u0000\u0000\u0000\t\n\u0001\u0000\u0000\u0000\n\u000b"+
		"\u0001\u0000\u0000\u0000\u000b\r\u0003\u0002\u0001\u0000\f\u000e\u0005"+
		"\f\u0000\u0000\r\f\u0001\u0000\u0000\u0000\r\u000e\u0001\u0000\u0000\u0000"+
		"\u000e\u000f\u0001\u0000\u0000\u0000\u000f\u0010\u0005\u0000\u0000\u0001"+
		"\u0010\u0001\u0001\u0000\u0000\u0000\u0011\u0012\u0005\u0003\u0000\u0000"+
		"\u0012\u0017\u0003\u0002\u0001\u0000\u0013\u0014\u0005\b\u0000\u0000\u0014"+
		"\u0016\u0003\u0002\u0001\u0000\u0015\u0013\u0001\u0000\u0000\u0000\u0016"+
		"\u0019\u0001\u0000\u0000\u0000\u0017\u0015\u0001\u0000\u0000\u0000\u0017"+
		"\u0018\u0001\u0000\u0000\u0000\u0018\u001e\u0001\u0000\u0000\u0000\u0019"+
		"\u0017\u0001\u0000\u0000\u0000\u001a\u001b\u0005\b\u0000\u0000\u001b\u001d"+
		"\u0003\u0006\u0003\u0000\u001c\u001a\u0001\u0000\u0000\u0000\u001d \u0001"+
		"\u0000\u0000\u0000\u001e\u001c\u0001\u0000\u0000\u0000\u001e\u001f\u0001"+
		"\u0000\u0000\u0000\u001f\u0085\u0001\u0000\u0000\u0000 \u001e\u0001\u0000"+
		"\u0000\u0000!\"\u0005\u0003\u0000\u0000\"\'\u0003\u0002\u0001\u0000#$"+
		"\u0005\b\u0000\u0000$&\u0003\u0002\u0001\u0000%#\u0001\u0000\u0000\u0000"+
		"&)\u0001\u0000\u0000\u0000\'%\u0001\u0000\u0000\u0000\'(\u0001\u0000\u0000"+
		"\u0000(.\u0001\u0000\u0000\u0000)\'\u0001\u0000\u0000\u0000*+\u0005\b"+
		"\u0000\u0000+-\u0003\u0006\u0003\u0000,*\u0001\u0000\u0000\u0000-0\u0001"+
		"\u0000\u0000\u0000.,\u0001\u0000\u0000\u0000./\u0001\u0000\u0000\u0000"+
		"/1\u0001\u0000\u0000\u00000.\u0001\u0000\u0000\u000012\u0005\u0007\u0000"+
		"\u00002\u0085\u0001\u0000\u0000\u000034\u0005\u0004\u0000\u000049\u0003"+
		"\u0002\u0001\u000056\u0005\b\u0000\u000068\u0003\u0002\u0001\u000075\u0001"+
		"\u0000\u0000\u00008;\u0001\u0000\u0000\u000097\u0001\u0000\u0000\u0000"+
		"9:\u0001\u0000\u0000\u0000:@\u0001\u0000\u0000\u0000;9\u0001\u0000\u0000"+
		"\u0000<=\u0005\b\u0000\u0000=?\u0003\u0006\u0003\u0000><\u0001\u0000\u0000"+
		"\u0000?B\u0001\u0000\u0000\u0000@>\u0001\u0000\u0000\u0000@A\u0001\u0000"+
		"\u0000\u0000A\u0085\u0001\u0000\u0000\u0000B@\u0001\u0000\u0000\u0000"+
		"CD\u0005\u0004\u0000\u0000DI\u0003\u0002\u0001\u0000EF\u0005\b\u0000\u0000"+
		"FH\u0003\u0002\u0001\u0000GE\u0001\u0000\u0000\u0000HK\u0001\u0000\u0000"+
		"\u0000IG\u0001\u0000\u0000\u0000IJ\u0001\u0000\u0000\u0000JP\u0001\u0000"+
		"\u0000\u0000KI\u0001\u0000\u0000\u0000LM\u0005\b\u0000\u0000MO\u0003\u0006"+
		"\u0003\u0000NL\u0001\u0000\u0000\u0000OR\u0001\u0000\u0000\u0000PN\u0001"+
		"\u0000\u0000\u0000PQ\u0001\u0000\u0000\u0000QS\u0001\u0000\u0000\u0000"+
		"RP\u0001\u0000\u0000\u0000ST\u0005\u0007\u0000\u0000T\u0085\u0001\u0000"+
		"\u0000\u0000UV\u0005\u0005\u0000\u0000V[\u0003\u0002\u0001\u0000WX\u0005"+
		"\b\u0000\u0000XZ\u0003\u0006\u0003\u0000YW\u0001\u0000\u0000\u0000Z]\u0001"+
		"\u0000\u0000\u0000[Y\u0001\u0000\u0000\u0000[\\\u0001\u0000\u0000\u0000"+
		"\\\u0085\u0001\u0000\u0000\u0000][\u0001\u0000\u0000\u0000^_\u0005\u0005"+
		"\u0000\u0000_d\u0003\u0002\u0001\u0000`a\u0005\b\u0000\u0000ac\u0003\u0006"+
		"\u0003\u0000b`\u0001\u0000\u0000\u0000cf\u0001\u0000\u0000\u0000db\u0001"+
		"\u0000\u0000\u0000de\u0001\u0000\u0000\u0000eg\u0001\u0000\u0000\u0000"+
		"fd\u0001\u0000\u0000\u0000gh\u0005\u0007\u0000\u0000h\u0085\u0001\u0000"+
		"\u0000\u0000ij\u0005\u0006\u0000\u0000jo\u0005\u0002\u0000\u0000kl\u0005"+
		"\b\u0000\u0000ln\u0003\u0006\u0003\u0000mk\u0001\u0000\u0000\u0000nq\u0001"+
		"\u0000\u0000\u0000om\u0001\u0000\u0000\u0000op\u0001\u0000\u0000\u0000"+
		"p\u0085\u0001\u0000\u0000\u0000qo\u0001\u0000\u0000\u0000rs\u0005\u0006"+
		"\u0000\u0000sx\u0005\u0002\u0000\u0000tu\u0005\b\u0000\u0000uw\u0003\u0006"+
		"\u0003\u0000vt\u0001\u0000\u0000\u0000wz\u0001\u0000\u0000\u0000xv\u0001"+
		"\u0000\u0000\u0000xy\u0001\u0000\u0000\u0000y{\u0001\u0000\u0000\u0000"+
		"zx\u0001\u0000\u0000\u0000{\u0085\u0005\u0007\u0000\u0000|\u007f\u0003"+
		"\u0004\u0002\u0000}~\u0005\f\u0000\u0000~\u0080\u0003\u0004\u0002\u0000"+
		"\u007f}\u0001\u0000\u0000\u0000\u0080\u0081\u0001\u0000\u0000\u0000\u0081"+
		"\u007f\u0001\u0000\u0000\u0000\u0081\u0082\u0001\u0000\u0000\u0000\u0082"+
		"\u0085\u0001\u0000\u0000\u0000\u0083\u0085\u0003\u0004\u0002\u0000\u0084"+
		"\u0011\u0001\u0000\u0000\u0000\u0084!\u0001\u0000\u0000\u0000\u00843\u0001"+
		"\u0000\u0000\u0000\u0084C\u0001\u0000\u0000\u0000\u0084U\u0001\u0000\u0000"+
		"\u0000\u0084^\u0001\u0000\u0000\u0000\u0084i\u0001\u0000\u0000\u0000\u0084"+
		"r\u0001\u0000\u0000\u0000\u0084|\u0001\u0000\u0000\u0000\u0084\u0083\u0001"+
		"\u0000\u0000\u0000\u0085\u0003\u0001\u0000\u0000\u0000\u0086\u0087\u0005"+
		"\u0001\u0000\u0000\u0087\u0089\u0005\t\u0000\u0000\u0088\u008a\u0005\n"+
		"\u0000\u0000\u0089\u0088\u0001\u0000\u0000\u0000\u0089\u008a\u0001\u0000"+
		"\u0000\u0000\u008a\u008c\u0001\u0000\u0000\u0000\u008b\u0086\u0001\u0000"+
		"\u0000\u0000\u008b\u008c\u0001\u0000\u0000\u0000\u008c\u008d\u0001\u0000"+
		"\u0000\u0000\u008d\u0098\u0005\u0002\u0000\u0000\u008e\u008f\u0005\u0001"+
		"\u0000\u0000\u008f\u0091\u0005\t\u0000\u0000\u0090\u0092\u0005\n\u0000"+
		"\u0000\u0091\u0090\u0001\u0000\u0000\u0000\u0091\u0092\u0001\u0000\u0000"+
		"\u0000\u0092\u0094\u0001\u0000\u0000\u0000\u0093\u008e\u0001\u0000\u0000"+
		"\u0000\u0093\u0094\u0001\u0000\u0000\u0000\u0094\u0095\u0001\u0000\u0000"+
		"\u0000\u0095\u0096\u0005\u0001\u0000\u0000\u0096\u0098\u0004\u0002\u0000"+
		"\u0001\u0097\u008b\u0001\u0000\u0000\u0000\u0097\u0093\u0001\u0000\u0000"+
		"\u0000\u0098\u0005\u0001\u0000\u0000\u0000\u0099\u009a\u0005\u0001\u0000"+
		"\u0000\u009a\u009b\u0005\u000b\u0000\u0000\u009b\u009c\u0005\u0001\u0000"+
		"\u0000\u009c\u0007\u0001\u0000\u0000\u0000\u0015\t\r\u0017\u001e\'.9@"+
		"IP[dox\u0081\u0084\u0089\u008b\u0091\u0093\u0097";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}