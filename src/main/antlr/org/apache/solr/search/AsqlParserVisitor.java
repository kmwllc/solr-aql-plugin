// Generated from org/apache/solr/search/AsqlParser.g4 by ANTLR 4.13.2
package org.apache.solr.search;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link AsqlParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface AsqlParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link AsqlParser#queryString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQueryString(AsqlParser.QueryStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code orOp}
	 * labeled alternative in {@link AsqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrOp(AsqlParser.OrOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andOp}
	 * labeled alternative in {@link AsqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndOp(AsqlParser.AndOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code groupedTermOp}
	 * labeled alternative in {@link AsqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGroupedTermOp(AsqlParser.GroupedTermOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notOp}
	 * labeled alternative in {@link AsqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotOp(AsqlParser.NotOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code impliedOp}
	 * labeled alternative in {@link AsqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImpliedOp(AsqlParser.ImpliedOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wordOrPhraseOp}
	 * labeled alternative in {@link AsqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWordOrPhraseOp(AsqlParser.WordOrPhraseOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code phraseOp}
	 * labeled alternative in {@link AsqlParser#wordOrPhrase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhraseOp(AsqlParser.PhraseOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wordOp}
	 * labeled alternative in {@link AsqlParser#wordOrPhrase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWordOp(AsqlParser.WordOpContext ctx);
}