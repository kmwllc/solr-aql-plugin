// Generated from org/apache/solr/search/AsqlLexer.g4 by ANTLR 4.13.2
package org.apache.solr.search;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast", "CheckReturnValue", "this-escape"})
public class AsqlLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.13.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		WORD=1, PHRASE=2, COLON=3, RANGE=4, LPAREN=5, RPAREN=6, OR=7, AND=8, IMPLIED=9, 
		NOT=10, WS=11, CR=12, EOF_RULE=13;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"WORD", "PHRASE", "COLON", "RANGE", "LPAREN", "RPAREN", "OR", "AND", 
			"IMPLIED", "NOT", "WS", "CR", "EOF_RULE"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "':'", null, "'('", "')'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "WORD", "PHRASE", "COLON", "RANGE", "LPAREN", "RPAREN", "OR", "AND", 
			"IMPLIED", "NOT", "WS", "CR", "EOF_RULE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public AsqlLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "AsqlLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\u0004\u0000\rh\u0006\uffff\uffff\u0002\u0000\u0007\u0000\u0002\u0001"+
		"\u0007\u0001\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004"+
		"\u0007\u0004\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007"+
		"\u0007\u0007\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b"+
		"\u0007\u000b\u0002\f\u0007\f\u0001\u0000\u0001\u0000\u0004\u0000\u001e"+
		"\b\u0000\u000b\u0000\f\u0000\u001f\u0003\u0000\"\b\u0000\u0001\u0001\u0003"+
		"\u0001%\b\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0004"+
		"\u0001+\b\u0001\u000b\u0001\f\u0001,\u0001\u0001\u0001\u0001\u0001\u0002"+
		"\u0001\u0002\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003\u0001\u0003"+
		"\u0003\u00038\b\u0003\u0001\u0004\u0001\u0004\u0001\u0005\u0001\u0005"+
		"\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006\u0001\u0006"+
		"\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007"+
		"\u0001\u0007\u0001\b\u0001\b\u0001\t\u0001\t\u0001\t\u0001\t\u0001\t\u0001"+
		"\t\u0001\n\u0004\nT\b\n\u000b\n\f\nU\u0001\u000b\u0005\u000bY\b\u000b"+
		"\n\u000b\f\u000b\\\t\u000b\u0001\u000b\u0004\u000b_\b\u000b\u000b\u000b"+
		"\f\u000b`\u0001\u000b\u0001\u000b\u0001\f\u0001\f\u0001\f\u0001\f\u0001"+
		",\u0000\r\u0001\u0001\u0003\u0002\u0005\u0003\u0007\u0004\t\u0005\u000b"+
		"\u0006\r\u0007\u000f\b\u0011\t\u0013\n\u0015\u000b\u0017\f\u0019\r\u0001"+
		"\u0000\u0004\n\u0000\t\n\r\r  \"\"()::<>[[]]\u3000\u3000\u0002\u0000<"+
		"<>>\u0006\u0000\t\n\r\r  \u200b\u200b\u3000\u3000\u8000\ufeff\u8000\ufeff"+
		"\u0002\u0000\n\n\r\rq\u0000\u0001\u0001\u0000\u0000\u0000\u0000\u0003"+
		"\u0001\u0000\u0000\u0000\u0000\u0005\u0001\u0000\u0000\u0000\u0000\u0007"+
		"\u0001\u0000\u0000\u0000\u0000\t\u0001\u0000\u0000\u0000\u0000\u000b\u0001"+
		"\u0000\u0000\u0000\u0000\r\u0001\u0000\u0000\u0000\u0000\u000f\u0001\u0000"+
		"\u0000\u0000\u0000\u0011\u0001\u0000\u0000\u0000\u0000\u0013\u0001\u0000"+
		"\u0000\u0000\u0000\u0015\u0001\u0000\u0000\u0000\u0000\u0017\u0001\u0000"+
		"\u0000\u0000\u0000\u0019\u0001\u0000\u0000\u0000\u0001!\u0001\u0000\u0000"+
		"\u0000\u0003$\u0001\u0000\u0000\u0000\u00050\u0001\u0000\u0000\u0000\u0007"+
		"7\u0001\u0000\u0000\u0000\t9\u0001\u0000\u0000\u0000\u000b;\u0001\u0000"+
		"\u0000\u0000\r=\u0001\u0000\u0000\u0000\u000fC\u0001\u0000\u0000\u0000"+
		"\u0011J\u0001\u0000\u0000\u0000\u0013L\u0001\u0000\u0000\u0000\u0015S"+
		"\u0001\u0000\u0000\u0000\u0017Z\u0001\u0000\u0000\u0000\u0019d\u0001\u0000"+
		"\u0000\u0000\u001b\"\u0005*\u0000\u0000\u001c\u001e\b\u0000\u0000\u0000"+
		"\u001d\u001c\u0001\u0000\u0000\u0000\u001e\u001f\u0001\u0000\u0000\u0000"+
		"\u001f\u001d\u0001\u0000\u0000\u0000\u001f \u0001\u0000\u0000\u0000 \""+
		"\u0001\u0000\u0000\u0000!\u001b\u0001\u0000\u0000\u0000!\u001d\u0001\u0000"+
		"\u0000\u0000\"\u0002\u0001\u0000\u0000\u0000#%\u0005-\u0000\u0000$#\u0001"+
		"\u0000\u0000\u0000$%\u0001\u0000\u0000\u0000%&\u0001\u0000\u0000\u0000"+
		"&*\u0005\"\u0000\u0000\'(\u0005\\\u0000\u0000(+\u0005\"\u0000\u0000)+"+
		"\t\u0000\u0000\u0000*\'\u0001\u0000\u0000\u0000*)\u0001\u0000\u0000\u0000"+
		"+,\u0001\u0000\u0000\u0000,-\u0001\u0000\u0000\u0000,*\u0001\u0000\u0000"+
		"\u0000-.\u0001\u0000\u0000\u0000./\u0005\"\u0000\u0000/\u0004\u0001\u0000"+
		"\u0000\u000001\u0005:\u0000\u00001\u0006\u0001\u0000\u0000\u000028\u0007"+
		"\u0001\u0000\u000034\u0005<\u0000\u000048\u0005=\u0000\u000056\u0005>"+
		"\u0000\u000068\u0005=\u0000\u000072\u0001\u0000\u0000\u000073\u0001\u0000"+
		"\u0000\u000075\u0001\u0000\u0000\u00008\b\u0001\u0000\u0000\u00009:\u0005"+
		"(\u0000\u0000:\n\u0001\u0000\u0000\u0000;<\u0005)\u0000\u0000<\f\u0001"+
		"\u0000\u0000\u0000=>\u0003\u0015\n\u0000>?\u0005O\u0000\u0000?@\u0005"+
		"R\u0000\u0000@A\u0001\u0000\u0000\u0000AB\u0003\u0015\n\u0000B\u000e\u0001"+
		"\u0000\u0000\u0000CD\u0003\u0015\n\u0000DE\u0005A\u0000\u0000EF\u0005"+
		"N\u0000\u0000FG\u0005D\u0000\u0000GH\u0001\u0000\u0000\u0000HI\u0003\u0015"+
		"\n\u0000I\u0010\u0001\u0000\u0000\u0000JK\u0003\u0015\n\u0000K\u0012\u0001"+
		"\u0000\u0000\u0000LM\u0005N\u0000\u0000MN\u0005O\u0000\u0000NO\u0005T"+
		"\u0000\u0000OP\u0001\u0000\u0000\u0000PQ\u0003\u0015\n\u0000Q\u0014\u0001"+
		"\u0000\u0000\u0000RT\u0007\u0002\u0000\u0000SR\u0001\u0000\u0000\u0000"+
		"TU\u0001\u0000\u0000\u0000US\u0001\u0000\u0000\u0000UV\u0001\u0000\u0000"+
		"\u0000V\u0016\u0001\u0000\u0000\u0000WY\u0003\u0015\n\u0000XW\u0001\u0000"+
		"\u0000\u0000Y\\\u0001\u0000\u0000\u0000ZX\u0001\u0000\u0000\u0000Z[\u0001"+
		"\u0000\u0000\u0000[^\u0001\u0000\u0000\u0000\\Z\u0001\u0000\u0000\u0000"+
		"]_\u0007\u0003\u0000\u0000^]\u0001\u0000\u0000\u0000_`\u0001\u0000\u0000"+
		"\u0000`^\u0001\u0000\u0000\u0000`a\u0001\u0000\u0000\u0000ab\u0001\u0000"+
		"\u0000\u0000bc\u0006\u000b\u0000\u0000c\u0018\u0001\u0000\u0000\u0000"+
		"de\u0005\u0000\u0000\u0001ef\u0001\u0000\u0000\u0000fg\u0006\f\u0000\u0000"+
		"g\u001a\u0001\u0000\u0000\u0000\n\u0000\u001f!$*,7UZ`\u0001\u0006\u0000"+
		"\u0000";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}