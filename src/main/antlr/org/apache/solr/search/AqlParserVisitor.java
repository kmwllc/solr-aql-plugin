// Generated from org/apache/solr/search/AqlParser.g4 by ANTLR 4.13.2
package org.apache.solr.search;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link AqlParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface AqlParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link AqlParser#queryString}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQueryString(AqlParser.QueryStringContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unmatchedParen}
	 * labeled alternative in {@link AqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnmatchedParen(AqlParser.UnmatchedParenContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andOp}
	 * labeled alternative in {@link AqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndOp(AqlParser.AndOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code orOp}
	 * labeled alternative in {@link AqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrOp(AqlParser.OrOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notOp}
	 * labeled alternative in {@link AqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotOp(AqlParser.NotOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code embeddedQueryOp}
	 * labeled alternative in {@link AqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEmbeddedQueryOp(AqlParser.EmbeddedQueryOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code doubleWordError}
	 * labeled alternative in {@link AqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDoubleWordError(AqlParser.DoubleWordErrorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wordOrPhraseOp}
	 * labeled alternative in {@link AqlParser#term}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWordOrPhraseOp(AqlParser.WordOrPhraseOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code phraseOp}
	 * labeled alternative in {@link AqlParser#wordOrPhrase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPhraseOp(AqlParser.PhraseOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code wordOp}
	 * labeled alternative in {@link AqlParser#wordOrPhrase}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWordOp(AqlParser.WordOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link AqlParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(AqlParser.ParameterContext ctx);
}