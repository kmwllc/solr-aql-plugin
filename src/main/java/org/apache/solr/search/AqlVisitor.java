package org.apache.solr.search;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.lucene.search.*;
import org.apache.solr.common.SolrException;
import org.apache.solr.request.SolrQueryRequest;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class AqlVisitor extends AqlParserBaseVisitor<Query> {
  private SolrQueryRequest req;

  public AqlVisitor(SolrQueryRequest req) {
    this.req = req;
  }

  public Query getQuery(String queryStr) throws SyntaxError {
    queryStr = queryStr.trim();
    Query result;
    CharStream input = CharStreams.fromString(queryStr);
    AqlLexer lexer = new AqlLexer(input);
    lexer.removeErrorListeners();
    lexer.addErrorListener(AqlLexerErrorListener.INSTANCE);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    AqlParser parser = new AqlParser(tokens);
    parser.removeErrorListeners();
    parser.addErrorListener(AqlParserErrorListener.INSTANCE);

    try {
      ParseTree tree = parser.queryString();
      result = this.visit(tree);
    } catch (RuntimeException e) {
      throw new SyntaxError(e.getMessage(), e.getCause());
    }

    if (result == null) {
      return new MatchNoDocsQuery();
    }

    return result;
  }

  @Override
  public Query visitQueryString(AqlParser.QueryStringContext ctx) {
    if (ctx.exception != null) {
      throw new RuntimeException("Syntax error at " + ctx.getText().substring(ctx.exception.getOffendingToken().getCharPositionInLine()));
    }
    return visit(ctx.term());
  }

  @Override
  public Query visitUnmatchedParen(AqlParser.UnmatchedParenContext ctx) {
    throw new RuntimeException(String.format("Syntax error: Unmatched Parenthesis after '%s'", ctx.getText()));
  }

  @Override
  public Query visitAndOp(AqlParser.AndOpContext ctx) {
    Map<String, String> paramMap = getParameterMap(ctx.parameter());

    BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
    if (ctx.term().size() == 0) {
      throw new RuntimeException("Syntax error after \"" + ctx.getText() + "\" for query \"" + ctx.getParent().getText() + "\"");
    }

    int clauseCount = 0;
    for (AqlParser.TermContext term : ctx.term()) {
      Query termQuery = visit(term);
      if (termQuery != null) {
        queryBuilder.add(termQuery, BooleanClause.Occur.MUST);
        clauseCount++;
      }
    }

    if (clauseCount == 0) {
      return null;
    }

    Query query = queryBuilder.build();

    if (paramMap.get("boost") != null) {
      float boostValue = Float.parseFloat(paramMap.get("boost"));
      query = new BoostQuery(query, boostValue);
    }

    return query;
  }

  @Override
  public Query visitOrOp(AqlParser.OrOpContext ctx) {
    Map<String, String> paramMap = getParameterMap(ctx.parameter());

    int mm = Integer.parseInt(paramMap.getOrDefault("minimum", "1"));
    if (mm < 1) {
      throw new RuntimeException("Parameter 'minimum' must be greater than zero");
    }

    BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
    if (ctx.term().size() == 0) {
      throw new RuntimeException("Syntax error after \"" + ctx.getText() + "\" for query \"" + ctx.getParent().getText() + "\"");
    }

    for (AqlParser.TermContext term : ctx.term()) {
      Query termQuery = visit(term);
      if (termQuery != null) {
        queryBuilder.add(termQuery, BooleanClause.Occur.SHOULD);
      }
    }
    queryBuilder.setMinimumNumberShouldMatch(mm);

    Query query = queryBuilder.build();
    if (((BooleanQuery)query).clauses().size() == 0) {
      return null;
    }

    if (paramMap.get("boost") != null) {
      float boostValue = Float.parseFloat(paramMap.get("boost"));
      query = new BoostQuery(query, boostValue);
    }

    return query;
  }

  @Override
  public Query visitNotOp(AqlParser.NotOpContext ctx) {
    Map<String, String> paramMap = getParameterMap(ctx.parameter());

    BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
    Query termQuery = visit(ctx.term());
    if (termQuery == null) {
      return null;
    }
    queryBuilder.add(termQuery, BooleanClause.Occur.MUST_NOT);
    queryBuilder.add(new BoostQuery(new MatchAllDocsQuery(), 0.0f), BooleanClause.Occur.MUST);
    //TODO:: check scoring here
    // queryBuilder.add(new MatchAllDocsQuery(), BooleanClause.Occur.MUST);

    Query query = queryBuilder.build();

    if (paramMap.get("boost") != null) {
      float boostValue = Float.parseFloat(paramMap.get("boost"));
      query = new BoostQuery(query, boostValue);
    }

    return query;
  }

  @Override
  public Query visitEmbeddedQueryOp(AqlParser.EmbeddedQueryOpContext ctx) {
    String phrase = ctx.PHRASE().getText();
    phrase = phrase.substring(1, phrase.length() - 1);
    phrase = phrase.replace("\\\"", "\"");

    Map<String, String> paramMap = getParameterMap(ctx.parameter());
    String defType = paramMap.getOrDefault("qlang", "edismax");
    try {
      QParser parser = QParser.getParser(phrase, defType, true, this.req);
      Query query = parser.getQuery();

      if (paramMap.get("boost") != null) {
        float boostValue = Float.parseFloat(paramMap.get("boost"));
        query = new BoostQuery(query, boostValue);
      }

      return query;
    } catch (SyntaxError syntaxError) {
      throw new RuntimeException(syntaxError);
    }
  }

  @Override
  public Query visitPhraseOp(AqlParser.PhraseOpContext ctx) {
    Query q = delegateParse(ctx.getText());
    if (q.toString().equals("+()")) { // TODO: this hack again
      return null;
    }

    return q;
  }
  
  @Override
  public Query visitWordOp(AqlParser.WordOpContext ctx) {
    Query q = delegateParse(ctx.getText());
    if (q.toString().equals("+()")) { // TODO: this hack again
      return null;
    }
    
    return q;
  }
  
  // Delegate lower level parsing to either asql or edismax
  private Query delegateParse(String q) {
    try {
      QParser parser;
      try {
        parser = QParser.getParser(q, "asql", true, this.req);
      } catch (SolrException ex) {
        parser = QParser.getParser(q, "edismax", true, this.req);
      }
      Query query = parser.getQuery();
      return query;
    } catch (SyntaxError e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }


  @Override
  public Query visitDoubleWordError(AqlParser.DoubleWordErrorContext ctx) {
    throw new RuntimeException("at: " + ctx.getParent().getText());
  }

  private Map<String, String> getParameterMap(List<AqlParser.ParameterContext> params) {
    Map<String, String> paramMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    for (AqlParser.ParameterContext parameter : params) {
      if (paramMap.get(parameter.paramName.getText()) != null) {
        throw new RuntimeException(String.format("parameter '%s' can't be used twice", parameter.paramName.getText()));
      }
      paramMap.put(parameter.paramName.getText(), parameter.paramValue.getText());
    }
    return paramMap;
  }

}
