package org.apache.solr.search;

import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * Created by dmeehl
 */
public class AqlParserErrorListener extends AqlLexerErrorListener {
  public static AqlParserErrorListener INSTANCE = new AqlParserErrorListener();

  @Override
  public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
    if (offendingSymbol instanceof CommonToken) {
      CommonToken token = (CommonToken)offendingSymbol;
      String lineStr = token.getTokenSource().getInputStream().toString();
      throwSyntaxError(lineStr, charPositionInLine);
    }
    
    throw new RuntimeException(msg);
  }
}
