package org.apache.solr.search;

import org.apache.solr.common.params.SolrParams;
import org.apache.solr.request.SolrQueryRequest;

public class AsqlQueryParserPlugin extends QParserPlugin {

  public QParser createParser(String q, SolrParams localParams, SolrParams params, SolrQueryRequest solrQueryRequest) {
    return new AsqlQParser(q, localParams, params, solrQueryRequest);
  }
  
}
