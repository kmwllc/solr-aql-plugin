package org.apache.solr.search;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * Created by dmeehl
 */
public class AqlLexerErrorListener extends BaseErrorListener {
  public static AqlLexerErrorListener INSTANCE = new AqlLexerErrorListener();
  protected static final int charMax = 30;

  @Override
  public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
    String lineStr = recognizer.getInputStream().toString();
    throwSyntaxError(lineStr, charPositionInLine++);
  }
  
  protected void throwSyntaxError(String lineStr, int charPositionInLine) {
    int leftPos = Math.max(0, charPositionInLine - charMax);
    leftPos = lineStr.substring(0, leftPos).lastIndexOf(" ");
    leftPos = Math.max(leftPos, 0);

    int rightPos = Math.min(charPositionInLine + charMax, lineStr.length());
    rightPos = charPositionInLine + charMax + lineStr.substring(rightPos).indexOf(" ") + 1;
    rightPos = Math.min(rightPos, lineStr.length());

    String lineBefore = leftPos == 0 ? lineStr.substring(0, charPositionInLine) : "..." + lineStr.substring(leftPos, charPositionInLine);
    String lineAfter = rightPos == lineStr.length() ? lineStr.substring(charPositionInLine) : lineStr.substring(charPositionInLine, rightPos) + "...";

    throw new RuntimeException(String.format("Syntax error at character %s: '%s‸%s'.", charPositionInLine, lineBefore, lineAfter));
  }
}
