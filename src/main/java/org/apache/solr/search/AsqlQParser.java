package org.apache.solr.search;

import org.apache.lucene.search.Query;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.parser.QueryParser;
import org.apache.solr.request.SolrQueryRequest;

public class AsqlQParser extends QParser {
  private AsqlVisitor asqlVisitor;
  
  
  public AsqlQParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
    super(qstr, localParams, params, req);

    String df = params.get(CommonParams.DF, "_text_");
    if (localParams != null) {
      this.qstr = localParams.get(CommonParams.VALUE, qstr);
      df = localParams.get(CommonParams.DF, df);
    }
    
    String op = req.getParams().get("q.op", "OR");
    if (localParams != null) {
      op = localParams.get("q.op", op);
    }
    
    this.asqlVisitor = new AsqlVisitor(req, df, QueryParser.Operator.valueOf(op), new ExtendedDismaxQParser.ExtendedSolrQueryParser(this, df));
  }

  @Override
  public Query parse() throws SyntaxError {
    
    return asqlVisitor.getQuery(qstr);
  }
}
