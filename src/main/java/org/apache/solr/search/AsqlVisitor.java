package org.apache.solr.search;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PackedTokenAttributeImpl;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.spans.*;
import org.apache.lucene.search.*;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.parser.QueryParser;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.schema.DateValueFieldType;
import org.apache.solr.schema.FieldType;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AsqlVisitor extends AsqlParserBaseVisitor<Query> {
  private final ExtendedDismaxQParser.ExtendedSolrQueryParser edismaxParser;
  private final QueryParser.Operator defOp;
  private String defaultField = "_text_";
  private SolrQueryRequest req;
  private QueryParser.Operator hlMode = null;
  private List<String> hlTerms = new ArrayList<>();
  
  private static Pattern WILDCARD_PATTERN = Pattern.compile("[^\\\\](\\*|\\?)");

  // Acceptable date formats.
  // The Solr format needs to be 1st.
  // Formats are interpreted as UTC unless timezone is specified.
  // Timezone can only be specified if format is at least hour precision.
  private static final DateFormat[] DATE_FORMATS = new DateFormat[]{
      new DateFormat("yyyy-M-d'T'HH:mm:ss.SSS'Z'", DateFormat.Precision.MILLISECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm:ss.SSS'Z\"'", DateFormat.Precision.MILLISECOND),
      new DateFormat("yyyy-M-d'T'HH:mm:ss.SSSz", DateFormat.Precision.MILLISECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm:ss.SSSz'\"'", DateFormat.Precision.MILLISECOND),
      new DateFormat("yyyy-M-d'T'HH:mm:ss.SSSZ", DateFormat.Precision.MILLISECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm:ss.SSSZ'\"'", DateFormat.Precision.MILLISECOND),
      new DateFormat("yyyy-M-d'T'HH:mm:ss.SSSX", DateFormat.Precision.MILLISECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm:ss.SSSX'\"'", DateFormat.Precision.MILLISECOND),
      new DateFormat("yyyy-M-d'T'HH:mm:ss.SSS", DateFormat.Precision.MILLISECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm:ss.SSS'\"'", DateFormat.Precision.MILLISECOND),
      new DateFormat("yyyy-M-d'T'HH:mm:ssX", DateFormat.Precision.SECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm:ssX'\"'", DateFormat.Precision.SECOND),
      new DateFormat("yyyy-M-d'T'HH:mm:ss", DateFormat.Precision.SECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm:ss'\"'", DateFormat.Precision.SECOND),
      new DateFormat("'\"'yyyy-M-d'T'HH:mmzzz'\"'", DateFormat.Precision.MINUTE),
      new DateFormat("yyyy-M-d'T'HH:mm", DateFormat.Precision.MINUTE),
      new DateFormat("'\"'yyyy-M-d'T'HH:mm'\"'", DateFormat.Precision.MINUTE),
      new DateFormat("yyyy-M-d'T'HHzzz", DateFormat.Precision.HOUR),
      new DateFormat("'\"'yyyy-M-d'T'HHzzz'\"'", DateFormat.Precision.HOUR),
      new DateFormat("yyyy-M-d'T'HH", DateFormat.Precision.HOUR),
      new DateFormat("'\"'yyyy-M-d'T'HH'\"'", DateFormat.Precision.HOUR),
      new DateFormat("yyyy-M-d", DateFormat.Precision.DAY),
      new DateFormat("'\"'yyyy-M-d'\"'", DateFormat.Precision.DAY),
      new DateFormat("yyyy/M/d", DateFormat.Precision.DAY), 
      new DateFormat("'\"'yyyy/M/d'\"'", DateFormat.Precision.DAY),
      new DateFormat("yyyy-M", DateFormat.Precision.MONTH),
      new DateFormat("'\"'yyyy-M'\"'", DateFormat.Precision.MONTH),
      new DateFormat("yyyy/M", DateFormat.Precision.MONTH),
      new DateFormat("'\"'yyyy/M'\"'", DateFormat.Precision.MONTH)
  };

  private static Pattern DATEMATH_PATTERN = Pattern.compile("^LAST(([1234567890]+)(DAYS|MONTHS|YEARS)|(MONTH|YEAR))");


  public AsqlVisitor(SolrQueryRequest req, String df, QueryParser.Operator defOp, ExtendedDismaxQParser.ExtendedSolrQueryParser edismaxParser) {
    this.req = req;
    if (df != null && !df.trim().equals("")) {
      this.defaultField = df;
    }
    this.edismaxParser = edismaxParser;
    this.defOp = defOp;
  }

  public Query getQuery(String queryStr) throws SyntaxError {
    queryStr = queryStr.trim();
    Query result;
    ANTLRInputStream input = new ANTLRInputStream(queryStr);
    AsqlLexer lexer = new AsqlLexer(input);
    lexer.removeErrorListeners();
    lexer.addErrorListener(AqlLexerErrorListener.INSTANCE);
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    AsqlParser parser = new AsqlParser(tokens);
    parser.removeErrorListeners();
    parser.addErrorListener(AqlParserErrorListener.INSTANCE);

    
    SolrParams originalParams = req.getOriginalParams();
    String originalHlq = originalParams.get("hl.q", null);
    
    // if hl is originally turned on but no explicit hl.q is passed in, we'll use our own.
    if (originalParams.getBool("hl", false) && originalHlq == null) {
      hlMode = QueryParser.Operator.valueOf(originalParams.get("hl.mode", QueryParser.Operator.OR.toString()));
    }
    
    try {
      ParseTree tree = parser.queryString();
      result = this.visit(tree);
    } catch (RuntimeException e) {
      throw new SyntaxError(e.getMessage(), e.getCause());
    }

    if (result == null) {
      return new MatchNoDocsQuery();
    }

    if (originalHlq == null) {
      if (hlTerms.size() > 0) {
        SolrParams params = req.getParams();
        if (params.get("hl.q") != null) {
          hlTerms.add(0, params.get("hl.q"));
        }
        ModifiableSolrParams modifiedParams = new ModifiableSolrParams(params);
        modifiedParams.set("hl.qparser", "lucene");
        String hlq = String.join(" ", hlTerms);
        if (!hlTerms.get(0).startsWith("{!edismax")) {
          hlTerms.add(0, String.format("{!edismax q.op=%s}", hlMode.toString()));
        }
        modifiedParams.set("hl.q", String.join(" ", hlTerms));
        req.setParams(modifiedParams);
      }
    }
    
    return result;
  }

  @Override
  public Query visitQueryString(AsqlParser.QueryStringContext ctx) {
    return visit(ctx.term());
  }

  @Override
  public Query visitGroupedTermOp(AsqlParser.GroupedTermOpContext ctx) {
    return visit(ctx.term());
  }

  @Override
  public Query visitImpliedOp(AsqlParser.ImpliedOpContext ctx) {
    BooleanClause.Occur occur = defOp == QueryParser.Operator.AND ? BooleanClause.Occur.MUST : BooleanClause.Occur.SHOULD;
    return handleOp(ctx.term(), occur);
  }

  @Override
  public Query visitOrOp(AsqlParser.OrOpContext ctx) {
    BooleanClause.Occur occur = BooleanClause.Occur.SHOULD;
    return handleOp(ctx.term(), occur);
  }

  @Override
  public Query visitAndOp(AsqlParser.AndOpContext ctx) {
    BooleanClause.Occur occur = BooleanClause.Occur.MUST;
    return handleOp(ctx.term(), occur);
  }

  @Override
  public Query visitNotOp(AsqlParser.NotOpContext ctx) {
    BooleanClause.Occur occur = BooleanClause.Occur.MUST_NOT;
    return handleOp(Collections.singletonList(ctx.term()), occur);
  }

  private Query handleOp(List<AsqlParser.TermContext> terms, BooleanClause.Occur occur) {
    BooleanQuery.Builder bldr = new BooleanQuery.Builder();

    if (BooleanClause.Occur.MUST_NOT.equals(occur)) {
      bldr.add(new MatchAllDocsQuery(), BooleanClause.Occur.MUST);
    }

    for (AsqlParser.TermContext term : terms) {
      Query q = visit(term);
      if (!q.toString().equals("+()")) { // TODO: this is a bit hacky
        bldr.add(q, occur);
      }
    }
    return bldr.build();
  }

  @Override
  public Query visitPhraseOp(AsqlParser.PhraseOpContext ctx) {
    String phrase = ctx.phrase.getText();

    // phrases containing wildcards
    if (WILDCARD_PATTERN.matcher(phrase).find()) {
      boolean negativeQuery = phrase.startsWith("-");
      phrase = phrase.substring(negativeQuery ? 2 : 1, phrase.length() - 1); // remove dash (not op) and unquote
      String field = getField(ctx.field);
      if (field.startsWith("-")) {
        field = field.substring(1);
        negativeQuery = !negativeQuery;
      }
      List<PackedToken> tokens = new ArrayList<>();
      int pos = 0;
      for (String term : phrase.split("\\s+")) {
        List<PackedToken> tokensForTerm = tokenize(field, term);
        for (PackedToken token : tokensForTerm) {
          token.position += pos; // update positions
        }
        tokens.addAll(tokensForTerm);
        int length = tokensForTerm.stream().map(token -> token.positionLength).reduce(1, Math::max);
        pos += length;
        
        if (hlMode != null) {
          hlTerms.add(term);
        }
      }
      
      if (negativeQuery) {
        BooleanQuery.Builder bldr = new BooleanQuery.Builder();
        bldr.add(new MatchAllDocsQuery(), BooleanClause.Occur.MUST);
        bldr.add(buildQuery(field, tokens), BooleanClause.Occur.MUST_NOT);
        return bldr.build();
      } else {
        return buildQuery(field, tokens);
      }
    }

    if (hlMode != null) {
      hlTerms.add(ctx.phrase.getText());
    }
    
    return processWordOrPhrase(ctx, ctx.field, ctx.range, ctx.phrase.getText());
  }

  @Override
  public Query visitWordOp(AsqlParser.WordOpContext ctx) {
    Matcher dateMathMatcher = DATEMATH_PATTERN.matcher(ctx.word.getText());
    if (dateMathMatcher.matches()) {
      String count = dateMathMatcher.group(2);
      if (count == null) count = "1";
      String unit = dateMathMatcher.group(3);
      if (unit == null) unit = dateMathMatcher.group(1);
      return delegateParse(getField(ctx.field) + ":[NOW/DAY-" + count + unit + " TO NOW]");
    }

    if (hlMode != null) {
      hlTerms.add(ctx.word.getText());
    }
    
    return processWordOrPhrase(ctx, ctx.field, ctx.range, ctx.word.getText());
  }

  private Query processWordOrPhrase(AsqlParser.WordOrPhraseContext ctx, Token fieldCtx, Token rangeOpCtx, String wordOrPhrase) {
    String field = getField(fieldCtx);

    // only convert date format for date fields
    if (req.getSchema().getFieldType(field) instanceof DateValueFieldType) {
      wordOrPhrase = convertDateSyntax(wordOrPhrase, rangeOpCtx);
    }

    if (rangeOpCtx != null) {
      wordOrPhrase = convertRangeSyntax(rangeOpCtx.getText(), wordOrPhrase);
    }

    if (fieldCtx == null) {
      return delegateParse(wordOrPhrase);
    } else {
      return delegateParse(fieldCtx.getText() + ":" + wordOrPhrase);
    }
  }

  // Returns the field represented by the Token or the default field.
  private String getField(Token fieldCtx) {
    return fieldCtx == null || fieldCtx.getText().length() == 0 ? defaultField : fieldCtx.getText();
  }

  // Converts from one of the known DATE_FORMATS into solr format maintaining precision
  // by rounding/truncating the formatted date to the precision level indicated by the format
  // that ultimately gets used. Returns a full range if range is true. Returns just the formatted
  // and rounded date if range is false.
  private String convertDateSyntax(String q, Token rangeOpCtx) {
    for (DateFormat format : DATE_FORMATS) {
      try {
        ZonedDateTime date = format.parse(q);
        String dateStr = DATE_FORMATS[0].format(date);

        // Exact match, If the format is ms precision.
        if (DateFormat.Precision.MILLISECOND.equals(format.precision)) {
          if (rangeOpCtx == null) {
            dateStr = "[" + dateStr + " TO " + dateStr + "]";
          } else if (rangeOpCtx.getText().equals(">")) {
            dateStr += "+1" + format.precision + "-1MILLISECOND";
          }
        }

        // otherwise, round/truncate to the precision level.
        else {
          if (rangeOpCtx == null) {
            dateStr = "[" + dateStr + " TO " + dateStr + "+1" + format.precision + "}";
          } else if (rangeOpCtx.getText().equals(">")) {
            dateStr += "+1" + format.precision + "-1MILLISECOND";
          }
        }

        return dateStr;
      } catch (DateTimeParseException e) {
        // move on to the next format
      }
    }

    // q is not a date (or not one we understand), so just pass it through.
    return q;
  }

  private String convertRangeSyntax(String op, String q) {
    String lower = op.contains("<") ? "*" : q;
    String upper = op.contains(">") ? "*" : q;
    String lowerBound = op.equals(">") ? "{" : "[";
    String upperBound = op.equals("<") ? "}" : "]";
    return lowerBound + lower + " TO " + upper + upperBound;
  }

  private Query delegateParse(String q) {
    try {
      QParser parser = QParser.getParser(q, "edismax", true, this.req);
      Query query = parser.getQuery();

      return query;
    } catch (SyntaxError e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  // Use the schema to tokenize the term and return a list of tokens with positional information.
  private List<PackedToken> tokenize(String field, String term) {
    FieldType fieldType = req.getSchema().getFieldType(field);
    List<PackedToken> packedTokens = new ArrayList<>();

    if (WILDCARD_PATTERN.matcher(term).find()) {
      PackedToken token = new PackedToken();
      token.term = term;
      token.wildcard = true;
      token.positionIncrement = 1;
      token.positionLength = 1;
      token.position = 1;
      packedTokens.add(token);
      return packedTokens;
    }

    try (Reader r = new StringReader(term)) {
      Analyzer fieldAnalyzer = fieldType.getQueryAnalyzer();
      TokenStream ts = fieldAnalyzer.tokenStream(field, r);
      CharTermAttribute charTermAttribute = ts.addAttribute(CharTermAttribute.class);
      ts.reset();

      int pos = 0;
      while (ts.incrementToken()) {
        PackedToken token = new PackedToken();
        token.term = charTermAttribute.toString();
        token.positionIncrement = ((PackedTokenAttributeImpl) charTermAttribute).getPositionIncrement();
        token.positionLength = ((PackedTokenAttributeImpl) charTermAttribute).getPositionLength();
        pos += token.positionIncrement;
        token.position = pos;
        packedTokens.add(token);
      }
      ts.close();
    } catch (IOException e1) {
      throw new RuntimeException(String.format("Exception tokenizing the query string: '%s'", term), e1);
    }

    return packedTokens;
  }

  // Use PackedToken's positional information to build the appropriate Query.
  private Query buildQuery(String field, List<PackedToken> tokens) {
    if (tokens.size() == 0) {
      return null;
    }

    // Optimize for only a single token
    if (tokens.size() == 1) {
      PackedToken token = tokens.get(0);
      if (token.wildcard) {
        return getWildcardQuery(field, token.term);
      }
      return new TermQuery(new Term(field, token.term));
    }

    List<List<PackedToken>> tokensByPosition = splitTokensByPosition(tokens);

    // Optimize if there's only 1 or 0 tokens at each position, And none of them have wildcards
    if (tokensByPosition.stream().map(positionList -> positionList == null || positionList.size() == 1).reduce(true, Boolean::logicalAnd)
        && tokens.stream().map(token -> !token.wildcard).reduce(true, Boolean::logicalAnd)
    ) {
      PhraseQuery.Builder phraseBuilder = new PhraseQuery.Builder();
      for (PackedToken token : tokens) {
        phraseBuilder.add(new Term(field, token.term), token.position);
      }
      return phraseBuilder.build();
    }

    // Optimize if there's only 1 position occupied.
    if (tokensByPosition.size() == 1) {
      BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
      for (PackedToken token : tokens) {
        if (token.wildcard) {
          queryBuilder.add(getWildcardQuery(field, token.term), BooleanClause.Occur.SHOULD);
        } else {
          queryBuilder.add(new TermQuery(new Term(field, token.term)), BooleanClause.Occur.SHOULD);
        }
      }
      return queryBuilder.build();
    }

    // Otherwise, we have to build a full SpanQuery.
    SpanNearQuery.Builder spanBldr = new SpanNearQuery.Builder(field, true);
    int maxPos = tokensByPosition.get(tokensByPosition.size() - 1).get(0).position;
    int maxPosConsumed = 0;
    while (maxPosConsumed < maxPos) {
      maxPosConsumed = buildSpanPhraseQuery(field, tokensByPosition, maxPosConsumed + 1, spanBldr);
    }
    SpanNearQuery query = spanBldr.build();
    if (query.getClauses().length == 1) {
      return query.getClauses()[0];
    }
    return query;
  }

  // Recursively adds clauses to the passed in SpanNearQuery.Builder by using positional and length information
  // to add either a gap, a SpanTermQuery or a SpanOrQuery.
  private int buildSpanPhraseQuery(String field, List<List<PackedToken>> tokensByPosition, int curPos, SpanNearQuery.Builder bldr) {
    List<PackedToken> tokensAtCurrentPos = tokensByPosition.get(curPos - 1);

    if (tokensAtCurrentPos == null) {
      bldr.addGap(1);
      return curPos;
    } else if (tokensAtCurrentPos.size() == 1) {
      PackedToken token = tokensAtCurrentPos.get(0);
      if (token.wildcard) {
        Query wildcardQuery = getWildcardQuery(field, token.term);
        if (wildcardQuery instanceof WildcardQuery) {
          bldr.addClause(new SpanMultiTermQueryWrapper<>((WildcardQuery) wildcardQuery));
        } else {
          // TODO:: handle cases where edismax doesnt return WildcardQuery
          throw new RuntimeException("Problem creating wildcard query.");
        }
      } else {
        bldr.addClause(new SpanTermQuery(new Term(field, token.term)));
      }
      return curPos + token.positionLength - 1;
    }

    List<SpanQuery> orTerms = new ArrayList<>();

    int longestTokenLength = 0;
    for (PackedToken token : tokensAtCurrentPos) {
      if (token.positionLength > longestTokenLength) {
        longestTokenLength = token.positionLength;
      }
    }

    for (PackedToken token : tokensAtCurrentPos) {
      if (token.positionLength < longestTokenLength) {
        SpanNearQuery.Builder orBldr = new SpanNearQuery.Builder(field, true);
        orBldr.addClause(new SpanTermQuery(new Term(field, token.term)));
        for (int i = curPos + token.positionLength; i < longestTokenLength + curPos; i++) {
          i = buildSpanPhraseQuery(field, tokensByPosition, i, orBldr);
        }
        orTerms.add(orBldr.build());
      } else {
        if (token.wildcard) {
          Query wildcardQuery = getWildcardQuery(field, token.term);
          if (wildcardQuery instanceof WildcardQuery) {
            orTerms.add(new SpanMultiTermQueryWrapper<>((WildcardQuery) wildcardQuery));
          } else {
            // TODO:: handle cases where edismax doesnt return WildcardQuery
            throw new RuntimeException("Problem creating wildcard query.");
          }
        } else {
          orTerms.add(new SpanTermQuery(new Term(field, token.term)));
        }
      }
    }

    // then add them to the OR
    bldr.addClause(new SpanOrQuery(orTerms.toArray(new SpanQuery[]{})));

    // and return the length of the packedTokens we've consumed.
    return curPos + longestTokenLength - 1;
  }

  // Returns a List of List of PackedToken where the index of the outer list is the (0-indexed) position
  // of the tokens in the inner Lists. i.e. Group by position with null elements as gaps.
  private static List<List<PackedToken>> splitTokensByPosition(List<PackedToken> tokens) {
    tokens.sort((a, b) -> a.position - b.position);
    List<List<PackedToken>> splitTokens = new LinkedList<>();
    int pos = 1;
    List<PackedToken> positionList = new LinkedList<>();
    splitTokens.add(positionList);
    for (PackedToken token : tokens) {
      if (token.position > pos) {
        for (int i = 1; i < token.position - pos; i++) {
          splitTokens.add(null); // add gaps when there are missing positions
        }
        positionList = new LinkedList<>();
        splitTokens.add(positionList);
        pos = token.position;
      }
      positionList.add(token);
    }
    return splitTokens;
  }

  // delegate to edismax for the wildcard
  private Query getWildcardQuery(String field, String term) {
    try {
      return edismaxParser.getWildcardQuery(field, term);
    } catch (SyntaxError e) {
      throw new RuntimeException(e.getMessage(), e);
    }
  }

  // token representation containing positional information
  private static class PackedToken {
    String term;
    int positionIncrement = 0;
    int positionLength = 0;
    int position = 0;
    boolean wildcard = false;

    public String toString() {
      return term;
    }
  }

  private static class DateFormat {
    enum Precision {
      YEAR, MONTH, DAY, HOUR, MINUTE, SECOND, MILLISECOND
    }

    private String fmtStr;
    private final DateTimeFormatter formatter;
    private final Precision precision;

    public DateFormat(String fmtStr, Precision precision) {
      this.fmtStr = fmtStr;
      this.formatter = DateTimeFormatter.ofPattern(fmtStr);
      this.precision = precision;
    }

    public String format(TemporalAccessor d) {
      return this.formatter.format(d);
    }

    // try to parse with zone, then with time, then just date
    public ZonedDateTime parse(String d) {
      try {
        return ZonedDateTime.parse(d, formatter).withZoneSameInstant(ZoneId.of("UTC"));
      } catch (DateTimeParseException e) {
        try {
          return ZonedDateTime.of(LocalDateTime.parse(d, formatter), ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of("UTC"));
        } catch (DateTimeParseException e2) {
          try {
            LocalDateTime localDate = LocalDate.parse(d, formatter).atStartOfDay();
            return ZonedDateTime.of(localDate, ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of("UTC"));
          } catch (DateTimeParseException e3) {
            LocalDateTime localDate = YearMonth.parse(d, formatter).atDay(1).atStartOfDay();
            return ZonedDateTime.of(localDate, ZoneId.of("UTC")).withZoneSameInstant(ZoneId.of("UTC"));
          }
        }
      }
    }
  }
}
