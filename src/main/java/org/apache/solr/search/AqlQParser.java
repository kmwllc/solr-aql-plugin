package org.apache.solr.search;

import org.apache.lucene.search.Query;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.request.SolrQueryRequest;

public class AqlQParser extends QParser {
  private AqlVisitor aqlVisitor;
  
  
  public AqlQParser(String qstr, SolrParams localParams, SolrParams params, SolrQueryRequest req) {
    super(qstr, localParams, params, req);

    String df = params.get(CommonParams.DF, "_text_");
    if (localParams != null) {
      this.qstr = localParams.get(CommonParams.VALUE, qstr);
      df = localParams.get(CommonParams.DF, df);
    }
    this.aqlVisitor = new AqlVisitor(req);
  }

  @Override
  public Query parse() throws SyntaxError {
    return aqlVisitor.getQuery(qstr);
  }
}
